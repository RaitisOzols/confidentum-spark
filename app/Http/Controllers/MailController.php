<?php

namespace App\Http\Controllers;

use App\Http\Requests\MessageRequest as Request;
use Mail;
use App\Mail\ContactRequest;
use App\Mail\SupportRequest;

class MailController extends Controller
{
    public function contact(Request $request)
    {
        Mail::to('spark@confidentum.lv')->send(new ContactRequest($request));

        return redirect()->back()->with('msg', 'thanks');
    }

    public function support(Request $request)
    {
        Mail::to('spark@confidentum.lv')->send(new SupportRequest($request));

        return redirect()->back()->with('msg', 'thanks');
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MessageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'antibot' => 'honeypot',
            'time' => 'required|honeytime:5',
            'name' => 'required|string|max:50',
            'company' => 'required|string|max:50',
            'email' => 'required|email|max:50',
            'phone' => 'nullable|string|max:30',
            'message' => 'required|string|max:1000',
        ];
    }
}

$('.calendly').on('click', function(e){

    e.preventDefault();

    $('.modal').removeClass('modal--visible');
    $('.modal__content').removeClass('modal__content--visible');

    Calendly.initPopupWidget({url: 'https://calendly.com/confidentum-spark/15min'});

    return false;
});

$(".switcher__button").on('click', function(e) {
    $(".pricing__toggle").toggleClass('pricing__signup--hidden');
});

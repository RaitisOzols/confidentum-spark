<!DOCTYPE html>
<html lang="lv" dir="ltr">

<head>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-154035955-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-154035955-1');
</script>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="author" content="Confidentum" />
    <meta name="description" content="Confidentum SPARK grāmatvedības pakalpojumu cenas" />
    <meta name="keywords" content="Grāmatvedības pakalpojumu cenas" />
    <title>Confidentum Spark</title>
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="css/swiper.css">
    <link rel="stylesheet" href="css/style.css">
    {{-- <link rel="stylesheet" href="css/custom.css"> --}}

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
    <link rel="manifest" href="/favicon/site.webmanifest">

    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,900" rel="stylesheet">

    <style media="screen">
        .card-title {
            padding: 0;
        }

        .card ul {
            list-style: initial;
            margin: initial;
            padding: 0 0 0 40px;
            margin-bottom: 20px;
        }


        .list-group .list-group-item {
            color: #005e59;
        }

        .list-group .list-group-item.active {
            background-color: #005e59;
            border-color: #005e59;
            color: white;
        }
    </style>

</head>

<body>

    <header class="header">
        <div class="header__content header__content--fluid-width">
            <div class="header__logo-title">
                <a href="/" style="color: inherit">
                    <span style="font-size: 0.75em">Confidentum</span>
                    SPARK
                </a>
            </div>
            <nav class="header__menu">
                <ul>
                    <li><a href="/#intro" class="selected header-link">SĀKUMS</a></li>
                    <li class="menu-item-has-children"><a href="/#features" class="header-link">SADAĻAS</a>
                        <ul class="sub-menu">
                            <li><a href="/#features" class="header-link">MŪSU PAKALPOJUMI</a></li>
                            <li><a href="/#about" class="header-link">KĀ TAS STRĀDĀ</a></li>
                            <li><a href="/#faq" class="header-link">ATBALSTS</a></li>
                            <li><a href="/#clients" class="header-link">MŪSU PARTNERI</a></li>

                        </ul>
                    </li>
                    <li><a href="/#pricing" class="header-link">CENAS</a></li>
                    {{-- <li><a href="/blog" class="header-link">AKADĒMIJA</a></li> --}}
                    <li><a href="/#support" class="header-link">KONTAKTI</a></li>
                    <li class="header__btn header__btn--login"><a href="javascript:void(0)" data-cb-type="portal">MANS SPARK</a></li>
                    <li class="header__btn header__btn--signup"><a href="/#pricing">GATAVS STARTAM</a></li>
                </ul>
            </nav>
        </div>
    </header>

    @yield('content')

    @include('partials/footer')

    <script src="../js/jquery-3.3.1.min.js"></script>
    <script src="../js/app.js"></script>
    <script src="../js/jquery.paroller.min.js"></script>
    <script src="../js/jquery.custom.js"></script>
    <script src="../js/swiper.min.js"></script>
    <script src="../js/swiper.custom.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            "use strict";
            if (jQuery.isFunction(jQuery.fn.paroller) && screen.width > 800) {
                $("[data-paroller-factor]").paroller();
            }

            $('.header').addClass('header--sticky');
            $('.header__menu ul ul').addClass('submenu-header-sticky');
        });
    </script>
    <script src="../js/custom.js"></script>

    @stack('js')

</body>

</html>

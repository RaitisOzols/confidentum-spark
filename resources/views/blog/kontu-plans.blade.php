@extends('blog.post')

@section('post-content')

<div class="card border-0">

<img src="/images/blog/coa.jpg" class="card-img-top" alt="...">

<div class="card-body pl-0 pr-0">

    <h5 class="grid__title">Kas ir kontu plāns un kāds Tev no tā labums?</h5>

    <div class="grid__text">

        <p>Kontu plāns ir visu kontu saraksts, ko uzņēmums izmanto finanšu pārskatu sagatavošanai.</p>

        <br>

        <b>Kāpēc kontu plāns ir tik svarīgs?</b>

        <p>Tas ir svarīgs, jo ar tā palīdzību var loģiski atdalīt izdevumus, ieņēmumus, aktīvus un saistības, lai uzņēmumam ir skaidri redzams tā finansiālais stāvoklis. Kontu plāns arī tiek izmantots grāmatvedības programmatūrā, lai apkopotu visus
        datus atskaitēs, kurus var izmantot uzņēmuma vadība un tiktu ievērota finanšu pārskatu un grāmatvedības likumdošana.</p>

        <br>

        <p>Kontu plāna standarta kategorijas ir:</p>
        <ul>
            <li>Aktīvi</li>
            <li>Saistības</li>
            <li>Pašu kapitāls</li>
            <li>Ieņēmumi</li>
            <li>Izdevumi</li>
        </ul>

        <p>Katrai no šīm kategorijām būs arī apakš kategorijas. Piemēram, izdevumu kategorijā var ietilpt:</p>

        <ul>
            <li>Pārdotās produkcijas ražošanas izmaksas</li>
            <li>Algas un personāla izmaksas</li>
            <li>Pārdošanas izmaksas</li>
            <li>Administrācijas izmaksas</li>
        </ul>

        <p>Kontu plāns ļauj katru darījumu pievienot kategorijai, lai precīzi redzētu, kur jūsu uzņēmums pelna un kur tērē naudu. Ir svarīgi pielāgot kontu plānu sava biznesa vajadzībām, jo dažādiem uzņēmumiem var būt būtiskas atšķirības pēc vajadzīgās informācijas. Un ir svarīgi, lai jums būtu izveidota konsekventa kontu plāna lietošanas sistēma, it īpaši, ja jūsu grāmatvedības programmā darījumus reģistrē vairāki cilvēki. Ir jābūt skaidram aprakstam, kurā kontā kuri darījumi ir jāiegrāmato, lai laika gaitā jūsu dati tiktu uzskaitīti konsekventi.</p>

        <br>


        <b>Bilance</b>

        <p>
            Bilance ir viens no biežāk izmantotajiem finanšu pārskatiem. Aktīvā ir uzskaitīti uzņēmumam piederosie lidzekli (piem. pamatlīdzekļi, krājumi, klientu parādi, nauda), savukārt Pasīvā ir redzami līdzekļu finansēšanas avoti (piem. īpašnieku ieguldījumi, peļņa, aizņēmumi, parādi piegādātājiem). Ir trīs dažādi bilances kontu veidi, kas jums jāzina:

            <ul>
                <li><b>Aktīvu konti.</b> Tajos tiek reģistrēti resursi, kas pieder jūsu uzņēmumam un kuriem ir vērtība. Tie var būt fiziski aktīvi, ieskaitot nekustamo īpašumu, aprīkojumu, naudu un pat nemateriālus priekšmetus, piemēram, preču zīmes un programmatūru.</li>
                <li><b>Saistību konti.</b> Šis ir visu jūsu uzņēmuma parādu reģistrs. Bieži vien šajā kategorijā atradīsit vārdu “parādi”, piemēram - parādi piegādātājiem.</li>
                <li><b>Kapitāla konti.</b> Tie ir līdzekļi, kas paliek pēc visu saistību atņemšanas no aktīviem. Pēc tā ir redzams cik liela daļa uzņēmumā pieder uzņēmuma īpašniekiem.</li>
            </ul>
        </p>

        <p>Konsekventa datu uzskaite palīdz tos salīdzināt laika gaitā, kas noved pie labākiem uzņēmējdarbības lēmumiem. Piemēram, aizdevuma atmaksa ietilpst saistībās, bet jūsu asistents reģistrē šos maksājumus izdevumu kategorijā, līdz ar to jūs nevarat pareizi novērtēt nevienu no šīm kategorijām un, iespējams, plēsīsiet ārā matus, mēģinot noskaidrot, kāpēc jūsu saistības nesamazinās, kamēr jūsu izdevumi šķiet ļoti lieli.</p>

        <br>

        <b>Izmaiņas kontu plānā</b>
        <p>Kontu plāna pielāgošana var palīdzēt iegūt nozīmīgākus finanšu datus. Tā kā jūsu bizness attīstās, ir svarīgi pārskatīt kontu plānu, lai pārliecinātos, ka tas joprojām ir aktuāls. Jums vajadzētu pārskatīt savu kontu plānu, lai redzētu, vai ir jāizveido, jārediģē vai jāarhivē kādus kontus, lai precīzāk atspoguļotu uzņēmuma darījumus. Jūs varat izveidot tik daudz kontu, cik vēlaties, taču nekad nevajadzētu izdzēst vecos kontus, jo tas var sajaukt jūsu grāmatvedību un radīt problēmas.</p>

        <br>

        <p>Lielākajai daļai jauno uzņēmumu kontu plāns laika gaitā attīstīsies, it īpaši veidojot to pirmo reizi un vēl tikai mēģinot saprast uzņēmuma patiesos ieņēmumus un izdevumus. Tomēr, laika gaitā, jums vajadzētu censties panākt, lai kontu plāns tiktu fiksēts un jūs varētu savākt pietiekami daudz finanšu datu. Tādējādi varēsiet precīzi tos salīdzināt un pieņemt pamatotus lēmumus.</p>

        <br>

        <b>Kopsavilkums</b>
        <p>Rezumējot, kontu plāns ir rīks, kas uzņēmumam nodrošina pilnīgu un precīzu darījumu uzskaiti. Konti ir sadalīti dažādās kategorijās. Tas ir labs veids, kā sakārtot finanses un sniegt plašāku ieskatu par uzņēmuma finansiālo stāvokli kopumā. Katram kontam ir nosaukums, īss konta apraksts un identifikācijas kods. Kontu plānu var pielāgot, lai tas atbilstu uzņēmuma individuālajām vajadzībām.</p>

    </div>

</div>

</div>

@php
    $ctaTitle = 'Vai Tev ir vajadzīga palīdzība ar kontu plānu?';

    $ctaText = 'Sazinies ar mums, ja Tev ir jautājumi par to, kā labāk strukturēt sava uzņēmuma konta plānu. Vai arī, ja meklē palīdzību grāmatvedībā, apskati mūsu piedāvājumu! Mēs labprāt palīdzēsim un nodrošināsim Tava uzņēmuma izaugsmi.';
@endphp

@endsection

@extends('blog.index')

@section('content')

    <section class="section">

        <div class="section__content section__content--fluid-width section__content--padding">

            <div class="container">

                <div class="row">

                    <h1 class="section__title section__title--centered">Grāmatvedības Akadēmija</h1>

                </div>

                <div class="row">
                    <div class="card-deck">
                    @for ($i=0; $i < 4; $i++)

                        <div class="card">
                            <img src="/images/blog/coa.jpg" class="card-img-top" alt="Kontu plāns">
                            <div class="card-body">
                                <h5 class="card-title">Card title</h5>
                                <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                            </div>
                        </div>

                    @endfor
                    </div>
                </div>

            </div>

        </div>

    </section>

@endsection

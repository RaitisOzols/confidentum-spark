@extends('blog.index')

@section('content')

<section class="section">

    <div class="section__content section__content--fluid-width section__content--about">

        <div class="row">
            <div class="col-4">
                <ul class="list-group">
                  <li class="list-group-item active"><b>Kas ir kontu plāns un kāds Tev no tā labums?</b></li>
                  <li class="list-group-item">Dapibus ac facilisis in</li>
                  <li class="list-group-item">Morbi leo risus</li>
                  <li class="list-group-item">Porta ac consectetur ac</li>
                  <li class="list-group-item">Vestibulum at eros</li>
                </ul>
            </div>
            <div class="col-8">

                @yield('post-content')

            </div>
        </div>

    </div>

</section>

<!-- Section -->
<section class="section section--clients">
        <svg class="svg-cta-top" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none">
            <path d="M0,70 C30,130 70,50 100,70 L100,100 0,100 Z" fill="#27ada6"/>
        </svg>
</section>

<section class="section section--cta">
        <div class="section__content section__content--fluid-width section__content--padding section__content--cta">

            <h2 class="section__title section__title--centered section__title--cta">{{$ctaTitle}}</h2>

            <div class="section__description section__description--centered section__description--cta">
                {{$ctaText}}
            </div>

            <div class="intro__buttons intro__buttons--centered">
            <a class="btn btn--orange-bg" href="/#pricing">SĀKT TAGAD</a>
            </div>
        </div>
        <svg class="svg-cta-bottom" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none">
            <path d="M0,70 C30,130 70,50 100,70 L100,100 0,100 Z" fill="#ffffff"/>
        </svg>
</section>

@endsection

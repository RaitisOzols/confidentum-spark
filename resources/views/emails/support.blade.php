@component('mail::message')
# Atbalsta pieteikums

Vārds: {{ $message->name }} <br>
Firma: {{ $message->company }} <br>
E-pasts: {{ $message->email }} <br>
Telefons: {{ $message->phone }} <br>
Ziņa: {{ $message->message }} <br>

@endcomponent

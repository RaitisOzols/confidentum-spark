@extends('legal.layout')

@section('content')
<h2>Lietošanas noteikumi</h2>

<p>
    <b>Laipni lūgti Confidentum SPARK!</b>
</p>

<p>
    Šie ir noteikumi vietnēm:
</p>
<ul>
    <li>
        <a href="https://spark.confidentum.lv/">https://spark.confidentum.lv</a>
    </li>
    <li>
        <a href="https://smartbooks.lv/">https://smartbooks.lv</a>
    </li>
    <li>
        <a href="https://algvedis.com/">https://algvedis.com</a>
    </li>
</ul>
<p>
    Šīs tīmekļa vietnes pieder <a href="https://confidentum.lv/" target="_blank"><b>Confidentum, SIA</b></a> reģ.nr.40003410841 un tiek izmantotas grāmatvedības pakalpojumu sniegšanai, turpmāk – <b>"Confidentum SPARK"</b>. Šie noteikumi attiecas uz vietni, programmatūru un Confidentum SPARK piedāvātajiem pakalpojumiem. Tie attiecas arī uz jebkuru datorā, mobilajā ierīcē, e-pastā vai citā veidā pieejamo Confidentum SPARK pakalpojumu versiju.
</p>

<h6>1. PIEKRIŠANA NOTEIKUMIEM</h6>

<p>
    Šajā līgumā aprakstīti juridiski saistoši Confidentum SPARK lietošanas
    noteikumi. Lietojot Confidentum SPARK, jūs piekrītat, ka jums ir saistošs
    šis līgums vai nu kā “Apmeklētājam” (kas nozīmē, ka jūs vienkārši
    pārlūkojat Confidentum SPARK vietni), vai “Lietotājam” (kas nozīmē, ka jūs
    esat reģistrējies Confidentum SPARK kā lietotājs). Ja jūs nepiekrītat šī
    līguma noteikumiem, nekavējoties izejiet no Confidentum SPARK vietnes un
    pārtrauciet pakalpojuma lietošanu. Līgumā periodiski var tikt veiktas
    izmaiņas, kuras stājas spēkā pēc to publicēšanas Confidentum SPARK.
    Turpinot lietot Confidentum SPARK pēc šādu izmaiņu publicēšanas, jūs
    piekrītat, ka jums ir saistošas jebkuras šajā līgumā veiktās izmaiņas,
    tādēļ ir svarīgi regulāri pārskatīt šo līgumu.
</p>
<p>
    Šis pakalpojums ir pieejams tikai uzņēmumiem. Tas nav paredzēts fiziskām
    personām. Jūs garantējat, ka visa jūsu iesniegtā reģistrācijas informācija
    ir pareiza un patiesa un ka jūsu pakalpojumu izmantojums neveido
    attiecināmo tiesību aktu pārkāpumu. Uzņēmums pēc saviem ieskatiem var
    atteikties piedāvāt pakalpojumu jebkurai juridiskai personai un mainīt
    atbilstības kritērijus jebkurā laikā.
</p>

<h6>2. PAKALPOJUMI</h6>

<p>
    Confidentum SPARK piedāvā nodokļu un grāmatvedības pakalpojumus maziem līdz vidēja lieluma
    uzņēmumiem ar tā vietnē pieejamās platformas starpniecību saskaņā ar šiem
    noteikumiem.
</p>
<p>
    Lietotājs sniedz savu finanšu informāciju, un platforma automātiski ģenerē
    grāmatvedības ierakstus. Lietotājam ir jāved un jāpārvalda grāmatvedības
    uzskaite pašam.
</p>
<p>
    Confidentum SPARK platforma arī pārbauda ierobežotu dokumentu skaitu,
    sagatavo algu sarakstu un aprēķina lietotāja nodokļus.
</p>
<p>
    Visi sniegtie pakalpojumi ir 100% digitāli un attālināti. Netiks piegādātas
    taustāmas preces vai faili.
</p>
<p>
    Confidentum SPARK piešķir lietotājam neekskluzīvas, atsaucamas tiesības
    izmantot Confidentum SPARK intelektuālo īpašumu un platformu tikai un
    vienīgi saistībā ar lietotāja darbībām pēc nepieciešamības.
</p>
<p>
    Lietotājam ir aizliegts nodot, pārdot, iznomāt, dot atļauju vai citādi
    padarīt Confidentum SPARK grāmatvedības pakalpojumus pieejamus trešām
    personām, ja vien nav skaidri noteikts citādi. Lietotājs nekādā gadījumā
    nedrīkst piedāvāt Confidentum SPARK pakalpojumus
    atsevišķi. Lietotājam ir jānodrošina šī līguma ietvaros sniegto Confidentum
    SPARK grāmatvedības pakalpojumu izmantojums
    atbilstoši attiecināmajiem tiesību aktiem un trešās personas tiesībām, kā
    arī šī līguma noteikumiem, tostarp Confidentum SPARK Privātuma politikai, kas kļūst
    par šī līguma sastāvdaļu. Lietotājam bez ierobežojuma jānodrošina, ka
    Confidentum SPARK ir tiesīgs izmantot lietotāja datus pēc nepieciešamības,
    lai sniegtu Confidentum SPARK pakalpojumus, un
    lietotājs nedrīkst izmantot Confidentum SPARK pakalpojumus tādā veidā, kas
    veidotu datu aizsardzības tiesību aktu, noteikumu, rīkojuma vai līdzīga
    likumdošanas akta pārkāpumu.
</p>
<p>
    Lietotājs vienīgais ir atbildīgs par visu mums sniegto lietotāja datu
    pareizību, atbilstību un pilnību. Confidentum SPARK izmanto iesniegtos
    lietotāja datus, sniedzot pakalpojumus, un nav atbildīgs par lietotāja datu
    pārskatīšanu, legalizēšanu vai citādu to pareizības, atbilstības vai
    pilnības apstiprināšanu. Confidentum SPARK sniegto pakalpojumu kvalitāte ir atkarīga no tā, cik pareiza ir jūsu
    sniegtā informācija.
</p>
<p>
    Lietotājs nedrīkst dekompilēt, disasemblēt vai citādi radīt, mēģināt radīt
    vai atvasināt vai ļaut vai palīdzēt jebkurai trešai personai radīt vai
    atvasināt jebkuras šī līgumu ietvaros lietotājam piešķirtās programmatūras
    pirmkodu, izņemot attiecināmo tiesību aktu atļautajā apmērā.
</p>

<h6>3. FAKTU ATSAUKUMS</h6>

<p>
    Mūsu pienākums ir palīdzēt jums vest pareizu uzņēmējdarbības grāmatvedības
    uzskaiti, taču mēs neesam atbildīgi par jūsu finanšu pārskatu datu ticamību
    un neauditēsim vai citādi nepārbaudīsim jūsu iesniegtos datus. Mēs
    nevērtējam un neesam atbildīgi par darījumu būtību, ticamību un nenesam
    atbildību par jūsu pieļautu dokumentu viltošanu, krāpšanu vai citām
    kriminālām darbībām, kā arī par darījumu vērtības atbilstību tirgus
    vērtībai, un jūsu veikto inventarizāciju patiesumu, tai skaitā par skaidrās
    naudas faktisko esamību kasē. Mūsu uzdevums ir iegrāmatot uzņēmuma finanšu
    darījumus, pamatojoties uz jūsu norādījumiem un ievaddatiem. Mēs
    centīsimies pielāgot grāmatvedības ierakstus vispārpieņemtiem grāmatvedības
    principiem un pareizai nodokļu lietvedībai, taču tas ir jūsu pienākums
    pārbaudīt mūsu veiktā darba pareizību.
</p>
<p>
    Mēs nekonstatēsim kļūdas, sagrozījumus, krāpniecību, pretlikumīgas darbības
    vai zādzību. Tādēļ mēs neesam iekļāvuši Confidentum SPARK platformā
    kārtību, kuras mērķis ir konstatēt šādas darbības, un jūs piekrītat, ka tā
    nav mūsu atbildība. Turklāt tikai un vienīgi jūs esat atbildīgi par
    dokumentu un grāmatvedības datu glabāšanu finanšu pārskatu pamatošanai
    audita gadījumā. Jums ir jāpārskata visi sagatavotie finanšu dati un
    dokumenti, pirms iesniedzat oficiālas atskaites nodokļu vai valsts
    iestādēs.
</p>
<p>
    Confidentum SPARK nav advokātu birojs vai tā aizvietotājs. Confidentum
    SPARK nevar un nesniedz jums nekāda veida juridiskos pakalpojumus, un
    Confidentum SPARK pakalpojumi neaizvieto un nevar aizvietot jurista
    konsultāciju vai juridiskus pakalpojumus.
</p>

<h6>4. LIETOTĀJA KONTS, PAROLE UN DROŠĪBA</h6>

<p>
    Reģistrējoties CONFIDENTUM SPARK, jums būs jāizvēlas parole un
    lietotājvārds un jums var lūgt papildu informāciju par jūsu kontu,
    piemēram, e-pasta adresi. Jūsu pienākums ir nodrošināt savas paroles un
    konta informācijas konfidencialitāti, un jūs esat pilnībā atbildīgs par
    visām darbībām, kuras notiek ar jūsu paroli vai kontu. Jūs piekrītat: (a)
    nekavējoties informēt Confidentum SPARK par nesankcionētu jūsu paroles vai
    konta izmantojumu vai jebkuru citu drošības pārkāpumu un (b) izrakstīties
    no sava konta katras sesijas beigās. Jūs nedrīkstat izmantot cita lietotāja
    kontu bez iepriekšējas Confidentum SPARK atļaujas. Confidentum SPARK
    neatbild par zaudējumiem un kaitējumu, kas rodas, jums rīkojoties pretrunā
    ar līguma noteikumiem.
</p>

<h6>5. MAKSĀJUMI UN MAKSAS</h6>

<p>
    Confidentum SPARK ir tiesīgs pieprasīt samaksu par atsevišķām pakalpojuma
    iespējām. Ja jūs abonējat šādas iespējas, jums tiks piemērotas visas
    attiecināmās maksas saskaņā ar vietnē aprakstīto. Confidentum SPARK ir
    tiesīgs mainīt izcenojumu un ieviest jaunas maksas jebkurā laikā, desmit
    (10) dienas iepriekš jūs par to brīdinot pa e-pastu vai publicējot
    informāciju vietnē. Ja jūs turpināt lietot pakalpojumu pēc šāda paziņojuma,
    tiks uzskatīts, ka esat pieņēmis jaunās vai paaugstinātās maksas.
</p>
<p>
    <b>Rēķinu piestādīšana</b>
    : Confidentum SPARK pakalpojumiem ir uzsākšanas un periodiska maksa, un jūs
    uzņematies atbildību par visām periodiskajām maksām pirms to atcelšanas.
    Jūs piekrītat, ka Confidentum SPARK var iekasēt ikmēneša maksas, nesaņemot
    no jums atsevišķu atļauju. Jūs varat atcelt šādas periodiskas maksas
    jebkurā laikā.
</p>
<p>
    <b>Maksājumi:</b>
    Lietotājam ir jānodrošina mums pilnīga un pareiza norēķinu un
    kontaktinformācija visā abonēšanas periodā, tostarp lietotāja vārds un
    uzvārds, uzņēmuma adrese un e-pasta adrese rēķiniem. Visus karšu maksājumus
    apstrādā trešās puses maksājumu pakalpojumu sniedzējs. Piekrītot šiem
    noteikumiem, jūs pilnvarojat mūs nosūtīt instrukcijas finanšu iestādei,
    kura izsniegusi jūsu karti, veikt maksājumus no jūsu kartes konta saskaņā
    ar šiem noteikumiem.
</p>
<p>
    <b>Nemaksāšana:</b>
    Mums nav jānodrošina jums pieeja pakalpojumam, ja tiek kavēta abonēšanas
    maksas vai jebkuras citas maksas, kuru jūs esat piekritis mums maksāt
    (tostarp ar pievienojumprogrammām vai papildu iespējām saistītās maksas).
</p>
<p>
    Ja tiek kavēta abonēšanas maksas vai citu saskaņoto maksājumu samaksa, mēs
    esam tiesīgi liegt piekļuvi pakalpojumam, līdz tiek samaksāts parāds, vai
    slēgt lietotāja Confidentum SPARK kontu. Tas
    attiecas arī uz gadījumiem, kad lietotāja maksājumu kartei ir beidzies
    termiņš.
</p>
<p>
    <b>Maksas par vairākiem kontiem:</b>
    Ja jūs vēlaties izmantot pakalpojumu, lai pārvaldītu vairākus uzņēmumus,
    jums būs jāreģistrē vairāki konti un jāmaksā abonēšanas maksa par katru
    papildu Confidentum SPARK kontu.
</p>

<h6>6. CONFIDENTUM SPARK KONTA SLĒGŠANA</h6>

<p>
    Šie noteikumi ir spēkā tikmēr, kamēr jūs turpināt piekļūt vai izmantot
    pakalpojumu, vai līdz jūs vai mēs izbeidzam šo noteikumu darbību jebkura,
    šajos noteikumos aprakstīta, iemesla dēļ.
</p>
<p>
    <b>Atcelšana no jūsu puses:</b>
    Jūs varat slēgt kontu jebkurā brīdī, piekļūstot pakalpojumam un izmantojot
    vietnes vai aplikācijas iespēju lapā “Mans SPARK”. Šajā gadījumā par nākamo
    mēnesi netiks aprēķināta abonēšanas maksa. Ņemiet vērā, ka jau piestādītās
    un samaksātās abonēšanas maksas netiks atmaksātas.
</p>
<p>
    <b>Apturēšana un slēgšana no mūsu puses:</b>
    Ja jūs (vai jebkurš cits konta lietotājs) neievēro šos pakalpojuma
    noteikumus vai iesaistās pretlikumīgās darbībās vai tiek kavēts abonēšanas
    maksas maksājums, mēs esam tiesīgi slēgt Confidentum SPARK kontu vai
    apturēt jūsu piekļuvi pakalpojumam. Ja mēs liedzam piekļuvi pakalpojumam šo
    noteikumu pārkāpuma dēļ, mēs neatlīdzināsim jūsu veiktos maksājumus.
    Turklāt mēs esam tiesīgi nekavējoties slēgt jebkuru kontu iepriekšminēto
    iemeslu vai jebkāda iemesla dēļ.
</p>

<h6>7. DROŠĪBAS KOMPONENTI</h6>

<p>
    Jūs apzināties, ka Confidentum SPARK un Confidentum SPARK programmatūra var
    ietvert drošības komponentus, kas ļauj aizsargāt digitālos materiālus, un
    ka uz šo materiālu izmantojumu attiecināmi Confidentum SPARK un/vai
    Confidentum SPARK platformas satura piegādātāju lietošanas noteikumi.
    Mēģināt neievērot, atspējot, apiet vai citādi iejaukties jebkurā šādā
    Confidentum SPARK drošības komponentā un lietošanas noteikumos ir
    aizliegts.
</p>

<h6>8. ĪPAŠUMTIESĪBAS</h6>

<p>
    Visi Confidentum SPARK materiāli, tostarp, bet ne tikai nosaukumi,
    logotipi, preču zīmes, attēli, teksts, slejas, grafikas, video, fotoattēli,
    ilustrācijas, grafika, programmatūra un citi elementi (kopā – “Materiāli”)
    tiek aizsargāti ar Confidentum SPARK vai trešo personu, kuras devušas
    atļauju vai citādi nodrošinājušas materiālu vietnei, piederošām un
    kontrolētām autortiesībām, preču zīmes un/vai citām intelektuālā īpašuma
    tiesībām. Jūs piekrītat, kad visi Materiāli ir pieejami Confidentum SPARK
    tikai ierobežota, nekomerciāla, personīga lietojuma nolūkā. Neviena fiziska
    vai juridiska persona nedrīkst nekādā veidā kopēt, reproducēt, pārpublicēt,
    pārdot, lejupielādēt, publicēt, nosūtīt vai izplatīt vai kā citādi izmantot
    Materiālus nekādā nolūkā bez iepriekšējas rakstiski skaidri izteiktas
    Confidentum SPARK atļaujas, izņemot ja tas skaidri norādīts šeit vai
    citviet Confidentum SPARK. Pievienot, dzēst, sagrozīt vai citādi mainīt
    Materiālus ir aizliegts. Jebkurš nesankcionēts mēģinājums mainīt
    Materiālus, manipulēt vai apiet jebkuru aizsardzības elementu vai izmantot
    Confidentum SPARK vai jebkuru Materiālu daļu jebkurā citā nolūkā ir stingri
    aizliegts.
</p>

<h6>9. NEKOMERCIĀLS IZMANTOJUMS</h6>

<p>
    Pakalpojumu nedrīkst izmantot komerciālos nolūkos, ja vien Confidentum
    SPARK to nav konkrēti atļāvis. Nesankcionēta “kadrēšana”, atsaukšanās vai
    saistīšana ar Confidentum SPARK ir aizliegta. Komerciālu reklāmu, saišu uz
    sadarbības partneriem un citu satura veidu publicēšanas gadījumā bez
    brīdinājuma var tikt atņemtas lietotāja privilēģijas.
</p>

<h6>10. CONFIDENTUM SPARK UN TREŠĀS PERSONAS</h6>

<p>
    Confidentum SPARK satur trešās puses licencētāju saturu Confidentum SPARK,
    kas ir aizsargāts ar autortiesībām, preču zīmju, patentu, profesionālo
    noslēpumu un citiem tiesību aktiem. Confidentum SPARK pieder un tas patur
    visas tiesības, īpašumtiesības un līdzdalību saturā. Confidentum SPARK ar
    šo piešķir jums ierobežotu, atsaucamu, nenododamu atļauju straumēt un/vai
    aplūkot saturu un jebkuru trešās puses saturu, kurš atrodas vai ir pieejams
    ar Confidentum SPARK vai pakalpojuma starpniecību (izņemot tajā esošos
    programmatūras kodus atbilstoši iepriekšminētajam), tikai un vienīgi
    personīgas, nekomerciālas lietošanas nolūkā attiecībā uz Confidentum SPARK
    aplūkošanu un pakalpojuma izmantojumu.
</p>
<p>
    Jebkuri darījumi ar trešām personām Confidentum SPARK, tostarp preču un
    pakalpojumu piegāde un apmaksa, vai jebkuri citi ar šādām trešām personām
    saistīti noteikumi vai garantijas ir tikai un vienīgi jūsu un attiecīgās
    trešās personas starpā. Confidentum SPARK nav atbildīgs par šādiem
    darījumiem vai veicināšanu. Atsauces uz jebkuru trešo personu vai jebkuras
    trešās personas precēm vai pakalpojumiem Confidentum SPARK nav uzskatāmas
    par šādas trešās personas vai šādas trešās personas preces vai pakalpojuma
    apstiprinājumu vai rekomendāciju no Confidentum SPARK vai tā darbinieku,
    amatpersonu, neatkarīgo darbuzņēmēju vai pārstāvju puses. Jebkura atsauce
    uz jebkuru trešo personu Confidentum SPARK tiek sniegta tikai informatīvā
    nolūkā. Confidentum SPARK aicina pašiem veikt šādu trešo personu un to
    preču un pakalpojumu izpēti un uzticamības pārbaudi. Confidentum SPARK
    strādā pie tā, lai nodrošinātu to, ka Confidentum SPARK informācija ir
    aktuāla un pareiza.
</p>

<h6>11. ATTEIKŠANĀS NO GARANTIJĀM</h6>

<p>
    JŪS PIEKRĪTAT LIETOT CONFIDENTUM SPARK TIKAI UN VIENĪGI UZ SAVU ATBILDĪBU.
    PAKALPOJUMI UN VISI MATERIĀLI (TURPMĀK KOPĀ – SATURS) TIEK SNIEGTI TO
    PAŠREIZĒJĀ STĀVOKLĪ UN BEZ JEBKĀDA VEIDA SKAIDRI VAI NETIEŠI SNIEGTĀM
    GARANTIJĀM. CONFIDENTUM SPARK, TĀ AMATPERSONAS, DIREKTORI, DARBINIEKI UN
    PĀRSTĀVJI TĀDĀ MĒRĀ, KĀDU TO ATĻAUJ LIKUMDOŠANA, ATSAKĀS SKAIDRI VAI
    NETIEŠI SNIEGT GARANTIJAS SAISTĪBĀ AR VIETNĒM UN TO IZMANTOJUMU NO JŪSU
    PUSES. CONFIDENTUM SPARK NESNIEDZ GARANTIJAS PAR CONFIDENTUM SPARK SATURA
    PAREIZĪBU VAI PILNĪBU UN NEUZŅEMTAS ATBILDĪBU PAR (I) SATURA KĻŪDĀM VAI
    NEPRECIZITĀTĒM, (II) JEBKĀDA VEIDA TRAUMĀM VAI KAITĒJUMU ĪPAŠUMAM JŪSU
    PIEKĻUVES VIETNEI VAI TĀS LIETOJUMA DĒĻ, (III) JEBKURU NESANKCIONĒTU
    PIEKĻUVI CONFIDENTUM SPARK DROŠAJIEM SERVERIEM UN/VAI UZ TIEM GLABĀTAJIEM
    PERSONAS DATIEM UN/VAI FINANŠU INFORMĀCIJAI VAI TO IZMANTOJUMU, (IV)
    JEBKURU SŪTĪJUMA CONFIDENTUM SPARK VAI NO TĀ PĀRTRAUKUMU, (V) JEBKURIEM
    VĪRUSIEM, TROJAS ZIRGIEM U. TML., KURUS JEBKURA TREŠĀ PERSONA VAR NOSŪTĪT
    CONFIDENTUM SPARK VAI AR TĀ STARPNIECĪBU UN/VAI (VI) JEBKURĀM SATURA KĻŪDĀM
    VAI IZLAIDUMIEM VAI JEBKURIEM ZAUDĒJUMIEM VAI KAITĒJUM, KAS RADIES JEBKURA
    AR CONFIDENTUM SPARK VIETŅU STARPNIECĪBU PUBLICĒTA, PA E-PASTU NOSŪTĪTA,
    NOSŪTĪTA VAI CITĀDI PAR PIEEJAMU PADARĪTA SATURA DĒĻ.
</p>

<h6>12. ATBILDĪBAS IEROBEŽOJUMS</h6>

<p>
    CONFIDENTUM SPARK, TĀ AMATPERSONAS, DIREKTORI, DARBINIEKI VAI PĀRSTĀVJI
    NEKĀDĀ GADĪJUMĀ NAV ATBILDĪGI JŪSU PRIEKŠĀ PAR TIEŠIEM, NETIEŠIEM,
    NEJAUŠIEM, ĪPAŠIEM, SODA VAI IZRIETOŠIEM ZAUDĒJUMIEM, KURI RADUŠIES
    TURPMĀKĀ DĒĻ: (I) SATURA KĻŪDAS VAI NEPRECIZITĀTES, (II) JEBKĀDA TRAUMA VAI
    ĪPAŠUMA BOJĀJUMI JŪSU PIEKĻUVES CONFIDENTUM SPARK UN TĀ LIETOJUMA DĒĻ,
    (III) JEBKURA NESANKCIONĒTA PIEKĻUVE CONFIDENTUM SPARK DROŠAJIEM SERVERIEM
    UN/VAI JEBKURIEM UZ TIEM GLABĀTĀJIEM PERSONAS DATIEM UN/VAI FINANŠU
    INFORMĀCIJAI VAI TO LIETOJUMS, (IV) JEBKURŠ SŪTĪJUMS UZ/NO CONFIDENTUM
    SPARK VAI SAKARU PĀRTRAUKUMS, (V) JEBKURI VĪRUSI, TROJAS ZIRGI U. TUML.,
    KURUS JEBKURA TREŠĀ PERSONAS VAR NOSŪTĪT CONFIDENTUM SPARK VAI AR TĀ
    STPARNIECĪBU UN/VAI (VI) JEBKURAS SATURA KĻŪDAS VAI JEBKURI ZAUDĒJUMI UN
    KAITĒJUMS, KAS RADIES JEBKURA AR CONFIDENTUM SPARK VIETŅU STARPNIECĪBU
    PUBLICĒTA, PA E-PASTU NOSŪTĪTA, NOSŪTĪTA VAI CITĀDI PAR PIEEJAMU PADARĪTA
    SATURA IZMANTOJUMA NO JŪSU PUSES, NEATKARĪGI NO TĀ, VAI TAS BALSTĪTS UZ
    GARANTIJU, LĪGUMU, LIKUMĀ NOTEIKTU ATLĪDZINĀMU KAITĒJUMU VAI JEBKURU CITU
    JURIDISKU TEORIJU UN NO TĀ, VAI CONFIDENTUM SPARK IR INFORMĒTS PAR ŠĀDU
    ZAUDĒJUMU IESPĒJAMĪBU. IEPRIEKŠMINĒTAIS ATBILDĪBAS IEROBEŽOJUMS ATTIECAS
    TĀDĀ MĒRĀ, KĀDU PIEĻAUJ LIKUMDOŠANA.
</p>
<p>
    JŪS ATZĪSTAT, KA CONFIDENTUM SPARK NAV ATBILDĪGS PAR LIETOTĀJU PUBLICĒTU
    SATURU VAI JEBKURAS TREŠĀS PERSONAS APMELOJOŠU, AIZVAINOJOŠU VAI
    PRETLIKUMĪGU RĪCĪBU UN KA ŠĀDAS RĪCĪBAS RADĪTĀ KAITĒJUMA VAI ZAUDĒJUMU
    RISKS GULSTAS TIKAI UZ JUMS.
</p>
<p>
    JEBKURA ATSAUCE UZ FIZISKU VAI JURIDISKU PERSONU, PRECI VAI PAKALPOJUMU
    CONFIDENTUM SPARK NAV UZSKATĀMA PAR APSTIPRINĀJUMU VAI REKOMENDĀCIJU NO
    CONFIDENTUM SPARK VAI JEBKURA TĀ DARBINIEKA PUSES. CONFIDENTUM SPARK NAV
    ATBILDĪGS PAR TREŠĀS PUSES SATURU VIETNĒS VAI NO CONFIDENTUM SPARK
    PIEKĻŪTĀS TREŠĀS PUSES VIETNĒS, UN CONFIDENTUM SPARK NEGARANTĒ TREŠĀS PUSES
    VIETNĒ ESOŠĀS INFORMĀCIJAS PAREIZĪBU VAI TĀS ATBILSTĪBU ATTIECĪGAJAM
    NOLŪKAM.
</p>
<p>
    NEKĀDA VEIDA KOMUNIKĀCIJA STARP JUMS UN CONFIDENTUM SPARK VAI CONFIDENTUM
    SPARK PĀRSTĀVI NEVEIDO ATTEIKUMU NO ATBILDĪBAS IEROBEŽOJUMA VAI PAPILDU
    GARANTIJU, KURA NAV SKAIDRI IZTEIKTA LIETOŠANAS NOTEIKUMOS.
</p>

<h6>13. NODOŠANA</h6>

<p>
    Jūs nedrīkstat nodot šo līgumu un ar to piešķirtās tiesības un atļaujas,
    taču Confidentum SPARK to drīkst bez ierobežojuma.
</p>

<h6>14. IEKĻAUŠANAS NOTEIKUMS</h6>

<p>
    Šis līgums kopā ar Privātuma politiku un jebkuru citu Confidentum SPARK
    publicētu juridisku paziņojumu veido visu vienošanos starp jums un
    Confidentum SPARK, kas regulē un attiecas uz regulē jūsu vietnes un
    pakalpojuma lietojumu.
</p>

<h6>15. NOTEIKUMU ATSAUKŠANA UN NODALĀMĪBA</h6>

<p>
    Ja Confidentum SPARK nerealizē vai nepiemēro kādu no šī līguma tiesībām vai
    nosacījumiem, tas nenozīmē, ka šādas tiesības vai nosacījums tiek atsaukts.
    Ja tiesa atzīst kādu no šī līguma punktiem par spēkā neesošu, puses
    vienojas, ka tiesai ir jācenšas realizēt pušu nodomus atbilstoši minētajam
    punktam. Tas neiespaidos pārējo šī līguma noteikumu spēkā esamību.
</p>

<h6>16. ATTIECINĀMĀ LIKUMDOŠANA </h6>

<p>
    Uz šo līgumu attiecināma Latvijas likumdošana. Jebkura prasība vai strīds,
    kas rodas starp jums un Confidentum SPARK, tiks izskatīts tikai un vienīgi
    Latvijas tiesā.
</p>

@endsection

<!DOCTYPE html>
<head>

<meta name="robots" content="noindex">

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<meta name="author" content="Confidentum" />
<title>Confidentum Spark</title>
<link rel="stylesheet" href="css/swiper.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,900">

<style media="screen">

p {
    margin-bottom: 10px;
}

h6 {
    padding-top: 10px;
}

ul, ol {
    display: block;
    list-style: disc outside none;
    margin: 1em 0;
    padding: 0 0 0 40px;
}

ol {
    list-style-type: decimal;
}

li {
    display: list-item;
    margin-top: 0.5em;
    line-height: normal;
}

h6 {
    padding-bottom: 5px;
    margin-top: 10px;
}

ol {
    list-style-type: none;
    counter-reset: item;
    margin: 0;
    padding: 0;
}

ol > li {
    display: table;
    counter-increment: item;
    margin-bottom: 0.6em;
}

ol > li:before {
    content: counters(item, ".") ". ";
    display: table-cell;
    padding-right: 0.6em;
}

li ol > li {
    margin: 0;
}

li ol > li:before {
    content: counters(item, ".") " ";
}

</style>
</head>
<body>

    <header class="header">
        <div class="header__content header__content--fluid-width">
            <div class="header__logo-title">
                <a href="/" style="color: inherit">
                    <span style="font-size: 0.75em">Confidentum</span>
                    SPARK
                </a>
            </div>
            <nav class="header__menu">
                <ul>
                    <li><a href="/#intro" class="selected header-link">SĀKUMS</a></li>
                    <li class="menu-item-has-children"><a href="/#features" class="header-link">SADAĻAS</a>
                        <ul class="sub-menu">
                            <li><a href="/#features" class="header-link">MŪSU PAKALPOJUMI</a></li>
                            <li><a href="/#about" class="header-link">KĀ TAS STRĀDĀ</a></li>
                            <!-- <li><a href="#testimonials" class="header-link">ATSAUKSMES</a></li> -->

                            <li><a href="/#faq" class="header-link">ATBALSTS</a></li>
                            <li><a href="/#clients" class="header-link">MŪSU PARTNERI</a></li>

                        </ul>
                    </li>
                    <li><a href="/#pricing" class="header-link">CENAS</a></li>
                    <li><a href="/#support" class="header-link">KONTAKTI</a></li>
                    <li class="header__btn header__btn--login"><a href="javascript:void(0)" data-cb-type="portal" >MANS SPARK</a></li>
                    <li class="header__btn header__btn--signup"><a href="/#pricing">GATAVS STARTAM</a></li>
                </ul>
            </nav>
        </div>
    </header>

    <section class="section">

        <div class="section__content section__content--fluid-width section__content--padding">

            <div class="container">

                @yield('content')

            </div>

        </div>

    </section>

    <section class="modal modal--signuplogin">
      <div class="modal__overlay modal__overlay--toggle"></div>
      <div class="modal__wrapper modal-transition">

      <div class="modal__body">

          @include('modals.support.contact-form')

      </div>

      </div>
    </section>    <!-- Modal for Login and Signup -->


<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/jquery.paroller.min.js"></script>
<script src="../js/jquery.custom.js"></script>
<script src="../js/swiper.min.js"></script>
<script src="../js/swiper.custom.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
    	"use strict";
    	if (jQuery.isFunction(jQuery.fn.paroller) && screen.width > 800 ) {
    		$("[data-paroller-factor]").paroller();
    	}

        $('.header').addClass('header--sticky');
        $('.header__menu ul ul').addClass('submenu-header-sticky');
    });
</script>

</body>
</html>

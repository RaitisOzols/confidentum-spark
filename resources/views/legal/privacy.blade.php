@extends('legal.layout')

@section('content')
<h2>PRIVĀTUMA POLITIKA</h2>

<p>
    <a href="https://confidentum.lv/" target="_blank"><b>Confidentum, SIA</b></a> reģ.nr.40003410841 augstu vērtē jūsu privātumu. Šajā dokumentā izklāstīts, kādu informāciju
    mēs iegūstam, kā mēs to izmantojam un, ja nepieciešams, kam mēs ar to
    dalāmies. Apmeklējot Confidentum SPARK tīmekļa vietni, jūs piekrītat šajā Privātuma politikā aprakstītajai praksei. Mūsu vietnes izmantojumam tiek piemēroti arī lietošanas noteikumi.
</p>

<h6>VISPĀRĪGĀ INFORMĀCIJA</h6>

<p>
    Par lietotāju personas datiem, kuri tiek iegūti un apstrādāti ar mūsu
    vietņu:
</p>
<ul>
    <li>
        <a href="https://spark.confidentum.lv/">https://spark.confidentum.lv</a>
    </li>
    <li>
        <a href="https://smartbooks.lv/">https://smartbooks.lv</a>
    </li>
    <li>
        <a href="https://algvedis.com/">https://algvedis.com</a>
    </li>
</ul>
<p>
    starpniecību, atbild:
</p>
<ul>
    <li>
        Confidentum SIA
    </li>
    <li>
        Krišjāņa Valdemāra 21-20, Rīga, LV-1010
    </li>
    <li>
        +371 67035353
    </li>
    <li>
        spark@confidentum.lv
    </li>
</ul>
<p>
    (turpmāk tekstā – “Confidentum SPARK”).
</p>

<h6>IEGŪTĀS INFORMĀCIJAS VEIDI</h6>

<p>
    Informācija, kuru mēs iegūstam no klientiem, palīdz personalizēt un
    nemitīgi uzlabot jūsu Confidentum SPARK lietošanas pieredzi. Mēs iegūstam
    šāda veida informāciju:
</p>
<p>
    <strong>Informācija, kuru jūs mums sniedzat.</strong>
    Jūs sniedzat informāciju, meklējot, skatoties, reģistrējoties, maksājot par
    grāmatvedības pakalpojumiem vai sazinoties ar mūsu klientu servisu. Šo
    darbību rezultātā jūs varat sniegt mums šādu informāciju:
</p>
<ul>
    <li>
        Vārds, uzvārds
    </li>
    <li>
        Adrese un pasta indekss
    </li>
    <li>
        Tālruņa numurs
    </li>
    <li>
        E-pasta adrese
    </li>
    <li>
        Bankas konta dati
    </li>
    <li>
        Nodokļu informācija
    </li>
    <li>
        Uzņēmējdarbības un finanšu informācija
    </li>
</ul>

<p>
    Mēs izmantojam jūsu sniegto informāciju jūsu pieprasītajos nolūkos,
    sniedzot grāmatvedības pakalpojumus ar platformas un vietnes starpniecību
    un sniedzot jaunus pakalpojumus. Confidentum SPARK neiegūs par jums personu
    identificējošu informāciju, ja vien jūs pats to nesniegsiet.
</p>
<p>
    <strong>Maksājumu informācija.</strong>
    Jūsu maksājumu kartes vai maksājuma informācija tiks apstrādāta ar
    maksājumu platformas palīdzību, nodrošinot jūsu datiem pilnīgu drošību un
    izmantojot tos tikai un vienīgi, lai samaksātu par Confidentum SPARK šajā
    vietnē sniegtajiem pakalpojumiem.
</p>
<p>
    <strong>GOOGLE Analytics. </strong>
    Mēs izmantojam Google, Inc., ASV (“Google”) rīku Google Analytics. Ar šo
    rīku un tehnoloģiju tiek iegūti un analizēti atsevišķi informācijas veidi,
    tostarp IP adreses, ierīču un programmatūras identifikatori, nosūtošie un
    izejošie URL, iespēju izmantojuma rādītāji un statistika, lietojuma un
    pirkumu vēsture, mediju piekļuves kontroles adrese (MAC adrese), unikālie
    mobilo ierīču identifikatori un cita līdzīga informācija ar sīkdatņu
    palīdzību. Google Analytics ģenerētā informācijas (tostarp IP adrese) var
    tikt nosūtīta Google, kas to var glabāt uz serveriem Amerikas Savienotajās
    Valstīs. Mēs izmantojam GOOGLE Analytics datu
    ieguvi, lai uzlabotu vietni un mūsu pakalpojumus.<strong></strong>
</p>
<p>
    <strong>Automātiskā informācija.</strong>
    Mēs līdzīgi kā daudz citu vietņu izmantojam sīkdatnes. Mēs iegūstam
    atsevišķus anonīmu datu veidus, kas nav personu identificējoši, jūsu
    interneta pārlūkam piekļūstot Confidentum SPARK vai trešās puses saturam,
    kuru apkalpo Confidentum SPARK vai tā vārdā citās vietnēs. Mēs iegūstam un
    analizējam šāda veida informāciju: interneta protokola (IP) adrese,
    informācija par datoru un pieslēgumu, piemēram, pārlūka veids, versija un
    pieslēguma ātrums, maksājumu vēsture un pakalpojumi un lapas, kuras jūs
    meklējāt, aplūkojāt un iespējams apmaksājāt.
</p>

<h6>KĀ MĒS IZMANTOJAM JŪSU INFORMĀCIJU </h6>

<p>
    Mēs izmantojam iegūto informāciju pirmām kārtām, lai nodrošinātu, uzturētu,
    aizsargātu un uzlabotu mūsu pašreizējo vietni un produktus un izstrādātu
    jaunus produktus. Mēs izmantojam ar vietnes palīdzību iegūtos personas
    datus atbilstoši turpmāk un citur politikas tekstā aprakstītajam, lai: <strong></strong>
</p>
<ul>
    <li>Reģistrētu grāmatvedības pakalpojumu lietojumu</li>
    <li>Izmantotu grāmatvedības pakalpojumus</li>
    <li>Uzlabotu mūsu produktus, vietni un to, kā mēs veicam savu uzņēmējdarbību</li>
    <li>Analītikas mērķiem ar Google Analytics starpniecību</li>
    <li>Saprastu un uzlabotu jūsu pieredzi, lietojot mūsu vietni un produktus</li>
    <li>Veiktu saziņu starp jums un mūsu klientu servisu</li>
    <li>Nosūtītu jums informāciju, tostarp apstiprinājumus, rēķinus, tehniskos
        paziņojumus, atjauninājumus, drošības brīdinājumus un atbalsta un
        administratīvas ziņas</li>
    <li>Sasaistītu jūsu informāciju ar citu informāciju, kuru mēs iegūstam no
        trešām personām, lai izprastu jūsu vajadzības un sniegtu jums labāku
        servisu</li>
    <li>Pasargātu, izskatītu un atturētu no krāpnieciskas, neatļautas vai
        pretlikumīgas darbības</li>
</ul>

<h6>KĀ JŪS SAŅEMAT MANU PIEKRIŠANU?</h6>

<p>
    Sniedzot mums personas datus, lai pabeigtu darījumu, samaksātu par
    pakalpojumiem un sazinātos ar mums, jūs sniedzat piekrišanu mums iegūt un
    izmantot jūsu informāciju. Lai atsauktu piekrišanu, sazinieties ar mums,
    izmantojot kontaktinformāciju.
</p>

<h6>SĪKDATNES</h6>

<p>
    Jums apmeklējot Confidentum SPARK vietni, mēs nosūtām vienu vai vairākas
    sīkdatnes.
</p>
<p>
    <strong>Kas ir sīkdatnes?</strong>
</p>
<p>
    “Sīkdatnes” ir nelielas teksta datnes, kuras tiek saglabātas jūsu datorā
    vai mobilajā ierīcē, apmeklējot vietni. Tās ļauj vietnei atpazīt jūsu
    ierīci un atcerēties, ja esat iepriekš apmeklējis šo vietni.
</p>
<p>
    Sīkdatnes ir plaši izmantota tīmekļa tehnoloģija. Vairums vietņu tās
    izmanto jau gadiem. Sīkdatnes tiek izmantotas, lai uzlabotu vietņu darbības
    efektivitāti, kā arī sniegtu informācija vietnes īpašniekiem.
</p>
<p>
    Tās tiek izmantotas, lai noteiktu, kuras vietnes daļas cilvēki apmeklē, un
    pielāgotu pieredzi katram lietotājam. Sīkdatnes arī sniedz informāciju,
    kura palīdz mums uzraudzīt un uzlabot vietnes darbību.
</p>
<p>
    <strong>
        Piekrišanas sīkdatņu izmantojumam noraidīšana vai atsaukšana
    </strong>
</p>
<p>
    Ja jūs nevēlaties, lai jūsu ierīcē tiktu saglabātas sīkdatnes, jūs varat
    iespējot savā interneta pārlūkā iestatījumu noraidīt visas vai dažas
    sīkdatnes un brīdināt jūs, kad jūsu ierīcē tiek saglabāta sīkdatne. Vairāk
    informācijas par to meklējiet pārlūka sadaļā Palīdzība (Help), Rīki (Tools)
    vai Rediģēt (Edit). Ņemiet vērā, ka, ja jūs bloķēsiet visas sīkdatnes,
    tostarp noteikti nepieciešamās sīkdatnes (skat. zemāk), iespējams, ka jūs
    nevarēsiet piekļūt vai izmantot visas vai daļu no CONFIDENTUM SPARK vietnes
    funkcijām.
</p>
<p>
    <strong>Mūsu sīkdatnes tiek izmantotas šādos nolūkos:</strong>
</p>
<p>
    Noteikti nepieciešamās: Šīm sīkdatnēm ir izšķiroši svarīga loma CONFIDENTUM
    SPARK vietnes pamatfunkciju pildīšanā. Piemēram, sīkdatnes, kuras ir
    nepieciešamas, lai reģistrētie lietotāji varētu autentificēties un veikt ar
    kontu saistītās funkcijas, kā arī saglabāt virtuālo “iepirkuma grozu”
    saturu vietnēs ar e-komercijas funkciju.
</p>
<p>
    Funkcionalitātes: Šīs sīkdatnes tiek izmantotas, lai saglabātu lietotāju
    iestatītās preferences, piemēram, konta nosaukums, valoda un atrašanās
    vieta. Tas ļauj CONFIDENTUM SPARK vietnei sniegt jums personalizētas
    iespējas.
</p>
<p>
    Drošības: Mēs izmantojam šīs sīkdatnes, lai noteiktu un novērstu
    potenciālos drošības apdraudējumus.
</p>
<p>
    Analītikas un veiktspējas (Google Analytics): Veiktspējas sīkdatnes iegūst
    informāciju par to, kā lietotāji mijiedarbojas ar mūsu vietnēm, tostarp
    kuras lapas tiek apmeklētas visvairāk, kā arī citus analītikas datus. Mēs
    izmantojam šos datus, lai uzlabotu to, kā mūsu vietnes funkcionē, un
    saprastu, kā lietotāji ar tām mijiedarbojas.
</p>

<h6>KĀ MĒS DALĀMIES INFORMĀCIJĀ</h6>

<p>
    Informācija par mūsu klientiem ir būtiska mūsu uzņēmējdarbības sastāvdaļa,
    un mēs ar to netirgojamies. Mēs dalāmies klientu informācijā, izņemot
    atbilstoši turpmāk aprakstītajos gadījumos.
</p>
<p>
    <strong>Trešās puses pakalpojumu sniedzēji.</strong>
    Mēs nolīgstam citus uzņēmumus un personas veikt funkcijas mūsu vārdā.
    Piemēram, uzlabot un uzturēt programmatūru, analizēt datus un apstrādāt
    karšu maksājumus. Tiem ir piekļuve personas datiem, kuri nepieciešami to
    funkciju pildīšanai, taču tie nedrīkst izmantot šos datus citos nolūkos.
</p>
<p>
    <strong>Uzņēmējdarbības pārcelšana.</strong>
    Gadījumā, ka Confidentum SPARK izveido, apvienojas vai to iegādājas cita
    juridiska persona, visticamāk, ka jūsu informācija tiks nodota. Confidentum
    SPARK nosūtīs jums e-pastu vai publicēs izceltu paziņojumu mūsu vietnē,
    pirms uz jūsu informāciju tiks attiecināta cita privātuma politika.
</p>
<p>
    <strong>Confidentum SPARK un citu personu</strong>
    <strong>aizsardzība</strong>
    . Mēs izpaužam konta un citus personas datus, kad mums tas šķiet atbilstoši
    likumdošanai, lai piemērotu vai attiecinātu mūsu noteikumus un citas
    vienošanās vai aizsargātu Confidentum SPARK, mūsu lietotāju vai citu
    personu tiesības, īpašumu vai drošību. Tas attiecas arī uz informācijas
    apmaiņu ar citiem uzņēmumiem un organizācijām, lai pasargātu no
    krāpniecības un mazinātu kredītrisku.
</p>
<p>
    <strong>Ar jūsu piekrišanu.</strong>
    Pārējos gadījumos jūs saņemsiet brīdinājumu, kad personu identificējoša
    informācija par jums var tikt nodota trešām personām, un jums tiks dota
    iespēja nepiekrist šādai darbībai.
</p>
<p>
    <strong>Anonīma informācija. </strong>
    Confidentum SPARK izmanto anonīmu pārlūkošanas informāciju, kuru mūsu
    serveri iegūst automātiski galvenokārt, lai palīdzētu mums pārvaldīt un
    uzlabot vietni.
</p>
<p>
    <strong>E-pasta adrese.</strong>
    E-pasta adrese, kuru jūs mums sniedzat e-pasta komunikācijas nolūkā, nekad
    netiks izīrēta vai pārdota trešām personām.
</p>

<h6>JŪSU INFORMĀCIJAS AIZSARDZĪBA</h6>

<p>
    Mēs strādājam pie jūsu informācijas drošības tās nosūtīšanas laikā,
    izmantojot drošligzdu slāņa (Secure Sockets Layer – SSL) programmatūru, kas
    šifrē jūsu ievadīto informāciju. Ja darījumi tiek apstrādāti vietnē,
    darījumu informācija tiek nosūtīta no un uz vietni šifrētā veidā,
    izmantojot nozares standarta SSL pieslēgumus, lai pasargātu to no
    pārtveršanas. Izmantojot šo šifrējumu, mēs nododam pilnu maksājumu kartes
    numuru attiecīgajam maksājumu kartes uzņēmumam pasūtījuma apstrādes laikā,
    taču pasūtījuma apstiprināšanas brīdī atklājot tikai pēdējos četrus
    maksājumu kartes numura ciparus. Mēs ierobežojam piekļuvi jūsu personas
    datiem, sniedzot to tikai personām ar leģitīmu nolūku to zināt, lai sniegtu
    jums preces vai pakalpojumus, un personām, kurām ir atļauts piekļūt šādai
    informācijai.
</p>
<p>
    Confidentum SPARK ievēro vispārpieņemtos nozares standartus, lai aizsargātu
    mums iesniegtos personas datus gan nosūtīšanas brīdī, gan tad, kad
    Confidentum SPARK tos saņem. Neviena nosūtīšanas metode internetā vai
    elektroniskās uzglabāšanas metode nav 100% droša. Tādēļ, lai gan
    Confidentum SPARK cenšas izmantot komerciāli pieņemamus līdzekļus, lai
    aizsargātu jūsu personas datus, mēs nevaram garantēt pilnīgu to drošību.
</p>
<p>
    Mēs nepārdosim, neizplatīsim un neiznomāsim jūsu personas datus trešām
    personām, izņemot ar jūsu piekrišanu vai atbilstoši tiesību aktiem.
</p>
<p>
    <h6>EIROPAS REZIDENTI</h6>
</p>
<p>
    Saskaņā ar VDAR (Vispārīgo datu aizsardzības regulu), ja jūs esat Eiropas
    rezidents, jums ir tiesības piekļūt mūsu rīcībā esošajiem jūsu personas
    datiem un pieprasīt, lai jūsu personas dati tiktu laboti, aktualizēti vai
    dzēsti. Ja jūs vēlaties realizēt šīs tiesības, sazinieties ar mums,
    izmantojot kontaktinformāciju.
</p>
<p>
    Turklāt, ja jūs esat Eiropas rezidents, mēs norādām, ka apstrādājam jūsu
    informāciju, lai izpildītu iespējamos līgumus ar jums, vai mūsu
    iepriekšminētajās leģitīmajās uzņēmējdarbības interesēs. Turklāt ņemiet
    vērā, ka jūsu informācija var tikt nodota ārpus Eiropas.
</p>

<h6>INFORMĀCIJAS REDIĢĒŠANA UN DZĒŠANA</h6>

<p>
    Ja jūs uzskatāt, ka mūsu rīcībā esošā informācija par jums ir nepareiza vai
    nepilnīga, rakstiet vai sūtiet mums e-pastu pēc iespējas ātrāk. Mēs
    nekavējoties labosim šādu informāciju. Jūs varat mainīt, labot, precizēt un
    dzēst savu informāciju jebkurā laikā, izņemot informāciju, kuru mēs esam
    ieguvuši, lai ievērotu nelikumīgi iegūtu līdzekļu legalizācijas apkarošanas
    noteikumus. Sazinieties ar mums, izmantojot kontaktinformāciju, lai
    iesniegtu pieprasījumu.
</p>
<p>
    Lai atrakstītos no Confidentum SPARK e-pasta saņemšanas, sekojiet e-pastā
    norādītajām instrukcijām. Jūsu pieprasījums tiks apstrādāts 48 stundu
    laikā.
</p>

<h6>PIEGĀDĀTĀJI UN CITAS TREŠĀS PERSONAS</h6>

<p>
    Šis dokuments attiecas tikai uz informācijas, kuru Confidentum SPARK iegūst
    par jums, izmantojumu un izpaušanu, ja vien šajā Privātuma politikā nav
    skaidri norādīts citādi. Ja jūs izpaužat savu informāciju citām personām –
    citiem Confidentum SPARK lietotājiem vai piegādātājiem, uz viņu veikto jūsu
    izpaustās informācijas izmantojumu vai izpaušanu var tikt attiecināti citi
    noteikumi. Confidentum SPARK nekontrolē trešo personu privātuma politikas,
    un uz jums attiecas šo trešo personu privātuma politikas, kur attiecināms.
    Confidentum SPARK neatbild par citu interneta vietņu privātuma vai drošības
    praksi, pat to, kurās ir saite uz <strong> </strong>Confidentum SPARK
    vietni un otrādi. Confidentum SPARK aicina jūs uzdot jautājumus, pirms
    izpaužat savus personas datus citām personām.
</p>

<h6>IZMAIŅAS ŠAJĀ PRIVĀTUMA POLITIKĀ</h6>

<p>
    Confidentum SPARK informācijas nolūkā publicēs datētas izmaiņas šajā
    Privātuma politikā. Confidentum SPARK ir tiesīgs veikt izmaiņas šajā
    Privātuma politikā jebkurā laikā, tādēļ iesakām to regulāri pārskatīt.
</p>

<h6>SAZINIETIES AR MUMS</h6>

<p>
    Ja jums ir jautājumi vai bažas par šo Privātuma politiku, kā arī jūsu datu
    apstrādi un drošību šajā vietnē, sazinieties ar mums:
</p>
<p>
    spark@confidentum.lv
</p>
<p>
    +371 67035353
</p>
<p>
    Krišjāņa Valdemāra 21, Rīga, LV-1010
</p>

@endsection

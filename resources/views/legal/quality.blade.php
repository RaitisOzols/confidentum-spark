@extends('legal.layout')

@section('content')
    <h2>Kvalitātes politika</h2>

    <p>
        SIA “Confidentum” kvalitātes politika ir veidota ar mērķi sniegt klientiem un sadarbības partneriem pārliecību par uzņēmuma sniegto pakalpojumu nemainīgi augsto kvalitāti un nodrošināt sistēmas atbilstību starptautiskajam kvalitātes pārvaldības sistēmas ISO 9001:2015 standartam.
    </p>
    <p>Kvalitātes politika ir vērsta uz:</p>
        <ul>
            <li><b>Klientiem</b> – esošo un potenciālo klientu vēlmju un prasību izzināšanu un apmierināšanu, atbilstoši visām normatīvajos aktos un citos dokumentos noteiktajām prasībām</li>
            <li><b>Darbiniekiem</b> – drošas un komfortablas darba vides nodrošināšanu, sniedzot iespējas pilnveidot un uzlabot savas zināšanas un profesionālo kompetenci, kā arī veicinot izpratni par kvalitātes vadības sistēmas prasībām un principiem.</li>
            <li><b>Kvalitātes</b> pārvaldības sistēmu un tās nepārtrauktu pilnveidošanu, efektīvu uzņēmuma resursu izmantošanu un uzņēmuma darbības efektivitātes paaugstināšanu.</li>
        </ul>
    <p>
        SIA “Confidentum” apņemas demonstrēt līderību un iesaistīšanos uzņēmuma kvalitātes vadības sistēmā, nodrošinot nepieciešamos resursus un veicinot procesu pieejas un uz riskiem balstītas domāšanas pielietošanu, lai kvalitātes vadības sistēma sasniegtu iecerētos rezultātus.
    </p>

    <p>Mūsu galvenie darbības pamatprincipi kvalitātes jomā ir:</p>

    <ul>
        <li>Konsekventi ievērot LR likumdošanas aktos un kvalitātes pārvaldības sistēmā noteiktās prasības.</li>
        <li>Noteikt un ievērot augstus kvalitātes standartus klientu apkalpošanā un pakalpojumu nodrošināšanā, tādējādi veicinot klientu apmierinātību ar uzņēmuma sniegtajiem pakalpojumiem.</li>
        <li>Ievērot klientu līgumos uzņemtās saistības un ārējo regulējošo likumdošanas aktu prasību kvalitatīvu izpildi.</li>
        <li>Pilnveidot darbinieku zināšanas un prasmes, veicinot darbinieku atbildību par sniegto pakalpojumu kvalitāti.</li>
        <li>Turpināt attīstīt uzņēmuma izstrādātās programmas un tehnoloģiskos risinājumus, kas palīdz optimizēt uzņēmuma klientu darbu un uzlabo pakalpojumu kvalitāti.</li>
        <li>Periodiski veikt uzņēmuma kvalitātes vadības sistēmas efektivitātes analīzi un ieviest nepieciešamos uzlabojumus.</li>
    </ul>
@endsection

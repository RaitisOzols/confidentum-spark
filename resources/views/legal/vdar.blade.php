@extends('legal.layout')

@section('content')
<h2>VDAR POLITIKA</h2>

<p>
    Saskaņā ar Eiropas Savienības Vispārīgo datu aizsardzības regulu
    Confidentum SPARK nodrošina šo VDAR politiku tādu lietotāju personas datiem
    un to darbinieku datiem, kuri atrodas Eiropas Ekonomikas zonā, kas aptver
    Eiropas Savienību, Islandi, Norvēģiju un Lihtenšteinu. Confidentum SPARK
    apņemas aizsargāt jūsu personas datus atbilstoši Vispārīgās datu
    aizsardzības regulas prasībām.
</p>
<p>
    Šis pielikums lasāms kopā ar Confidentum SPARK
    Privātuma politiku. Šajā pielikumā sniegta papildu informācija par mūsu
    veiktajām apstrādes darbībām un jūsu tiesībām saskaņā ar ES VDAR.
</p>
<p>
    Mēs neapstrādāsim īpašas (jeb sensitīvas) personas datu kategorijas vai
    personas datus, kuri saistīti ar notiesājošiem spriedumiem vai noziedzīgiem
    nodarījumiem, izņemot gadījumos, kur to pieļauj attiecināmā likumdošana,
    vai ar personas skaidri izteiktu piekrišanu.
</p>

<h6>KURŠ VEIC JŪSU UN DARBINIEKU PERSONAS DATU APSTRĀDI?</h6>

<p>
    Par lietotāja un tā darbinieku personas datiem, kuri tiek iegūti un
    apstrādāti ar tīmekļa vietņu:
</p>
<ul>
    <li>
        <a href="https://spark.confidentum.lv/">https://spark.confidentum.lv</a>
    </li>
    <li>
        <a href="https://smartbooks.lv/">https://smartbooks.lv</a>
    </li>
    <li>
        <a href="https://algvedis.com/">https://algvedis.com</a>
    </li>
</ul>
<p>
    starpniecību, atbild:
</p>
<ul>
    <li>
        Confidentum SIA
    </li>
    <li>
        Krišjāņa Valdemāra 21-20, Rīga, LV-1010
    </li>
    <li>
        +371 67035353
    </li>
    <li>
        spark@confidentum.lv
    </li>
</ul>
<p>
    (turpmāk tekstā – “Confidentum SPARK”).
</p>

<h6>INFORMĀCIJAS UN TĀS IZMANTOJUMA VEIDI</h6>

<p>
    Confidentum SPARK izmanto jūsu sniegto informāciju par darbiniekiem jūsu pieprasītajā nolūkā
    un nodrošina grāmatvedības pakalpojumus ar Confidentum SPARK platformas un
    pakalpojuma starpniecību.
</p>
<p>
    No lietotāja piešķirtajām datu bāzēm mēs iegūstam šādu informāciju:
</p>
<ul>
    <li>
        darbinieka vārds, uzvārds;
    </li>
    <li>
        personas kods;
    </li>
    <li>
        dzīvesvietas adrese;
    </li>
    <li>
        darbinieka atalgojums;
    </li>
    <li>
        līguma veids;
    </li>
    <li>
        bankas konta dati;
    </li>
    <li>
        e-pasts.
    </li>
</ul>
<p>
    Mēs nepārdosim jūsu personas datus citiem uzņēmumiem un nenodosim tos
    citiem uzņēmumiem lietošanā bez jūsu piekrišanas.
</p>

<h6>UZGLABĀŠANAS TERMIŅŠ</h6>

<p>
    Jūsu sniegtā informācija par jūsu darbiniekiem tiek glabāta uz serveriem,
    kurus pārvalda un uztur Confidentum SPARK un trešās personas, pamatojoties
    uz līgumu ar Confidentum SPARK. Confidentum SPARK glabās jūsu datus tikai
    tik ilgi, cik būs pamatoti nepieciešams, lai izpildītu Privātuma politikā
    aprakstītos mērķus un, ja attiecināms, risinātu jebkuru pretenziju vai
    strīdu, kas var rasties saistībā ar jebkuriem pakalpojumiem, kurus jūs
    izmantojat ar mūsu tīmekļa vietnes starpniecību.
</p>

<h6>JŪSU PIEKRIŠANAS SAŅEMŠANA</h6>

<p>
    Ja jūsu personas datu izmantojumam mums ir nepieciešama jūsu piekrišana,
    jūs varat sniegt šādu piekrišanu:
</p>
<ul>
    <li>
        brīdī, kad mēs iegūstam jūsu personas datus, atbilstoši sniegtajiem
        norādījumiem;
    </li>
    <li>
        informējot mūs pa e-pastu, pastu vai tālruni, izmantojot šajā politikā
        sniegto kontaktinformāciju;
    </li>
    <li>
        lietotājs apgalvo, ka tam ir darbinieku piekrišana apstrādāt viņu
        datus.
    </li>
</ul>
<p>
    Ņemiet vērā, ka, ja jūs sniedzat piekrišanu jūsu personas datu papildu
    izmantojumam, mēs varam izmantot jūsu personas datus atbilstoši sniegtajai
    piekrišanai.
</p>

<h6>JŪSU PERSONAS DATU KONFIDENCIALITĀTE UN DROŠĪBA</h6>

<p>
    Mēs apņemamies nodrošināt jūsu sniegto darbinieku personas datu drošību, un
    veiksim piesardzības pasākumus, lai pasargātu darbinieku personas datus no
    zuduma, ļaunprātīgas izmantošanas vai pārveidošanas.
</p>

<h6>JŪSU TIESĪBAS</h6>

<p>
    Saskaņā ar ES VDAR jums un jūsu darbiniekiem ir šādas tiesības, kuras
    attiecināmas atsevišķos apstākļos un kurām tiek piemēroti atsevišķi
    ierobežojumi, attiecībā uz jūsu sniegtajiem darbinieku personas datiem:
</p>
<ul>
    <li>tiesības saņemt informāciju par jūsu personas datu ieguvi un lietojumu;</li>
    <li>tiesības piekļūt saviem personas datiem;</li>
    <li>tiesības iebilst pret savu personas datu apstrādi atsevišķos apstākļos;</li>
    <li>tiesības precizēt savus personas datus, ja tie ir nepareizi vai nepilnīgi;</li>
    <li>tiesība pieprasīt, lai jūsu personas dati tiktu dzēsti;</li>
    <li>tiesības ierobežot veidus, kādos mēs apstrādājam jūsu personas datus;</li>
    <li>tiesības nodot vai iegūt savu personas datu kopiju viegli pieejamā formātā;</li>
    <li>tiesības atsaukt piekrišanu;</li>
    <li>tiesības nesniegt piekrišanu atsevišķiem automatizētiem lēmumu pieņemšanas procesiem;</li>
    <li>tiesības vērsties ar sūdzību uzraudzības iestādē.</li>
</ul>

<p>
    Darbinieku personas datu apstrādes pamatā ir no darbiniekiem iepriekš
    iegūta piekrišana. Darbinieki ir tiesīgi atsaukt šādu piekrišanu jebkurā
    laikā.
</p>

<h6>KONTAKTI, JŪSU TIESĪBU REALIZĒŠANA UN SŪDZĪBAS</h6>

<p>
    Ja jums ir jautājumi vai bažas par šo VDAR politiku vai jūs vēlaties uzdot
    jautājumus vai izteikt bažas par darbinieku personas datu ieguvi,
    pārvaldību un apstrādi, vai realizēt savas tiesības, sazinieties ar mums:
</p>
<p>
    spark@confidentum.lv
</p>
<p>
    +371 67035353
</p>
<p>
    Krišjāņa Valdemāra 21, Rīga, LV-1010
</p>

@endsection

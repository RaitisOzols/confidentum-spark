<section class="modal modal--animation">
 <div class="modal__overlay modal__overlay--toggle"></div>
     <div class="modal__wrapper modal__wrapper--image modal-transition">
         <div class="modal__body">
             <button class="modal__close modal__overlay--toggle"><span></span></button>
             <div class="modal__header">Kā tas strādā</div>

             <div class="modal__image">
                 <img src="images/intro-animation.gif" alt="Sertificēti tiešsaistes grāmatvedības pakalpojumi par fiksētu cenu bez operāciju skaita ierobežojuma." title="Confidentum SPARK grāmatvedības pakalpojumi maziem un vidējiem uzņēmumiem"/>
             </div>
         </div>
     </div>
</section>    <!-- Modal for animation -->

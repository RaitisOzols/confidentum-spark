<div class="modal__content modal__content--forgot">
    <div class="modal__form form">
        <h2 class="form__title">Aizmirsi paroli?</h2>
            <form class="form__container" id="ForgotForm" method="post" action="index.html">
            <div class="form__row">
                <label class="form__label">Epasts</label>
                <input name="emailf" class="form__input" type="text"/>
                <span class="form__row-border"></span>
            </div>
            <input type="submit" name="submit" class="form__submit btn btn--green-bg" id="submitf" value="ATJAUNOT PAROLI" />
            </form>
    </div>
    <div class="modal__info">
        <h2 class="modal__title">Mēs palīdzēsim</h2>
        <div class="modal__descr">Paroles atjaunošanas saite tiks nosūtīta uz epastu.</div>
        <button class="modal__switch modal__switch--signup" data-popup="login">Atcerējos paroli</button>
    </div>
</div>  <!-- End Modal login -->

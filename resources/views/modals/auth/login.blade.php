<div class="modal__content modal__content--login">
    <div class="modal__info">
        <h2 class="modal__title">Vai pirmo reizi šeit?</h2>
        <div class="modal__descr">Pievienojies tagad un iegūsti <span>BEZMAKSAS</span> datu pārnešanu</div>
        <ul class="modal__list">
        <li>premium access to all products</li>
        <li>free testing tools</li>
        <li>unlimited user accounts</li>
        </ul>
        <button class="modal__switch modal__switch--signup" data-popup="signup">Reģistrēties</button>
    </div>
    <div class="modal__form form">
        <h2 class="form__title">Pieslēgties</h2>
            <form class="form__container" id="LoginForm" method="post" action="index.html">

            <!-- <div class="form__row">
                    <label class="form__label" for="namec">Vārds</label>
                <input name="namec" id="namec" class="form__input" type="text"/>
                <span class="form__row-border"></span>
            </div> -->
            <div class="form__row">
                    <label class="form__label">Epasts</label>
                <input name="email_login" class="form__input" type="text"/>
                <span class="form__row-border"></span>
            </div>
            <div class="form__row">
                <label class="form__label" for="pass_login">Parole</label>
                <input name="pass_login" id="pass_login" class="form__input" type="password"/>
                <span class="form__row-border"></span>
            </div>

            <div class="modal__checkbox"><input id="remember" name="remember" value="remember" checked type="checkbox"><label for="remember">Atcerēties mani</label></div>
            <div class="modal__switch modal__switch--forgot" data-popup="forgot">Aizmirsi paroli?</div>
            <input type="submit" name="submit" class="form__submit btn btn--green-bg" id="submitl" value="PIESLĒGTIES" />
            </form>
    </div>
</div>  <!-- End Modal login -->

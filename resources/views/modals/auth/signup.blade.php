<div class="modal__content modal__content--signup">
    <div class="modal__form form">
        <h2 class="form__title">Reģistrēties</h2>
            <form class="form__container" id="SignupForm" method="post" action="index.html">
            <!-- <div class="form__row">
                <label class="form__label" for="names">Username</label>
                <input name="namec" id="names" class="form__input" type="text"/>
                <span class="form__row-border"></span>
            </div> -->
            <div class="form__row">
                <label class="form__label">Epasts</label>
                <input name="email_signup" class="form__input" type="text"/>
                <span class="form__row-border"></span>
            </div>
            <div class="form__row">
                <label class="form__label" for="pass_signup">Parole</label>
                <input name="pass_signup" id="pass_signup" class="form__input" type="password"/>
                <span class="form__row-border"></span>
            </div>
            <input type="submit" name="submit" class="form__submit btn btn--green-bg" id="submit" value="REĢISTRĒTIES" />
            </form>
    </div>
    <div class="modal__info">
        <h2 class="modal__title">Vai jau esi reģistrējies?</h2>
        <div class="modal__descr">Pieslēdzies tagad un sāc lietot mūsu <span>lielisko</span> pakalpojumu</div>
        <ul class="modal__list">
        <li>premium access to all products</li>
        <li>free testing tools</li>
        <li>unlimited user accounts</li>
        </ul>
        <button class="modal__switch modal__switch--login" data-popup="login">Pieslēgties</button>
    </div>
</div>  <!-- End Modal signup -->

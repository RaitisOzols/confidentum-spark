<section class="modal modal--signuplogin">
  <div class="modal__overlay modal__overlay--toggle"></div>
  <div class="modal__wrapper modal-transition">

  <div class="modal__body">

      @include('modals/auth/login')

      @include('modals/auth/forgot')

      @include('modals/auth/signup')

      @include('modals/support/contact-form')

      @include('modals/support/support-form')

      @include('modals/thank-you')

  </div>

  </div>
</section>

@push('js')
<script type="text/javascript">

$(document).ready(function(){

    let msg = {!! json_encode(session('msg') ?? '') !!};

    if (msg == 'thanks') {
        $('.modal--signuplogin').toggleClass('modal--visible');
        $('.modal__content--thank-you-form').toggleClass('modal__content--visible');
    }
});

</script>
@endpush

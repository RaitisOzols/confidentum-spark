<div class="modal__content modal__content--support-form">
    <div class="modal__form form">
        <h2 class="form__title">Vai Tev ir vajadzīga palīdzība?</h2>
            <form class="form__container" id="SupportForm" method="post" action="{{route('support-form')}}">

                @csrf

                {!! Honeypot::generate('antibot', 'time') !!}

                <div class="form__row">
                        <label class="form__label" for="support_form_name">Vārds</label>
                    <input name="name" id="support_form_name" class="form__input" type="text" required/>
                    <span class="form__row-border"></span>
                </div>
                <div class="form__row">
                        <label class="form__label" for="support_form_company">Uzņēmums</label>
                    <input name="company" id="support_form_company" class="form__input" type="text" required/>
                    <span class="form__row-border"></span>
                </div>
                <div class="form__row">
                    <div style="width: 45%; display: inline-block; margin-right: 9%">
                        <label class="form__label">E-pasts</label>
                        <input name="email" class="form__input" type="email" required/>
                        <span class="form__row-border form__row-border_email"></span>
                    </div>

                    <div style="width: 45%; display: inline-block;">
                        <label class="form__label">Telefons</label>
                        <input name="phone" class="form__input" type="text"/>
                        <span class="form__row-border form__row-border_phone"></span>
                    </div>
                </div>

                <div class="form__row">
                    <label class="form__label" for="support_form_message">Jautājums</label>
                    <textarea class="form__input" name="message" id="support_form_message" required style="resize: vertical;"></textarea>
                    <span class="form__row-border"></span>
                </div>

                <input type="submit" name="submit" class="form__submit btn btn--green-bg" id="support_form_submit" value="NOSŪTĪT ZIŅU" />
            </form>
    </div>
    <div class="modal__info">
        <h2 class="modal__title">Pieteikt bezmaksas konsultāciju</h2>
        <div class="modal__descr">Spied zemāk, lai <span>rezervētu</span> piemērotāko <span>sarunas laiku</span>.</div>
        <ul class="modal__list">
            <li>Izvēlies sev piemērotu laiku</li>
            <li>Uzzini kā SPARK var Tev palīdzēt</li>
            <li>Saņem bezmaksas konsultāciju</li>
        </ul>
        <button class="modal__switch modal__switch--login calendly">Pieteikt konsultāciju</button>
    </div>
</div>  <!-- End Modal signup -->

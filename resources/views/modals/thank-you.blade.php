<div class="modal__content modal__content--thank-you-form">
    <div class="modal__form form">
        <h2 class="form__title">Paldies!</h2>
            <form class="form__container" id="ThankYouForm" method="post" action="index.html">
                <div class="form__row">
                    <p>Ziņu esam saņēmuši un drīzumā ar Tevi sazināsimies!</p>
                </div>
            </form>
    </div>
    <div class="modal__info" style="width: 42%">
        <h2 class="modal__title">Confidentum SPARK</h2>
        {{-- <div class="modal__descr">Pieslēdzies tagad un sāc lietot mūsu <span>lielisko</span> pakalpojumu</div> --}}
        <ul class="modal__list">
            <li>Piekļūsti intelektuālajam kapitālam</li>
            <li>Fiksētas grāmatvedības izmaksas</li>
            <li>Attīsti uzņēmumu ar mūsdienīgu pieeju</li>
        </ul>
        <button class="modal__switch modal__switch--login modal--close">Aizvērt</button>
    </div>
</div>

@push('js')
<script type="text/javascript">

$('.modal--close').on('click', function(){
    $('.modal').removeClass('modal--visible');
    $('.modal__content').removeClass('modal__content--visible');
});

</script>
@endpush

<!-- Section -- about -->
<section class="section" id="about">

        <div class="section__content section__content--fluid-width section__content--about">
            <div class="grid grid--5col grid--about">

                <div class="grid__item grid__item--x2">
                    <h3 class="grid__title">Kāpēc <span>jaunie uzņēmumi izvēlas</span> SPARK?</h3>
                    <p class="grid__text">SPARK palīdz jaunajiem uzņēmumiem koncentrēties uz viņu darbu un dalīties ar obligātās dokumentācijas, grāmatvedības un ar to saistīto pienākumu nastu. Tas ir mūsu veids, kā vienkāršot un padarīt jaunajiem uzņēmumiem pieejamāku grāmatvedības ekspertu atbalstu un optimizēt izmaksas. Tavi ieguvumi strādājot ar mums ir:</p>
                    <ul class="grid__list">
                        <li>Tavs personīgais grāmatvedības eksperts</li>
                        <li>Piekļuve jaudīgai <a href="https://smartbooks.lv/" target="_blank"><b>biznesa vadības programmai</b></a></li>
                        <li>Kvalitatīvi pakalpojumi ar optimālām izmaksām</li>
                        <li>Grāmatvedības ekspertu konsultācijas, lai izpildītu VID un likumdošanas prasības</li>
                    </ul>

                </div>
                <div class="grid__item grid__item--x3">
                    <div class="grid__image grid__image--right" data-paroller-factor="0.2" data-paroller-type="foreground" data-paroller-direction="vertical"><img src="images/desktop-frame-about.png" alt="Mazie un vidējie uzņēmumi izvēlas Confidentum SPARK grāmatvedības pakalpojumus." title="Confidentum SPARK grāmatvedības pakalpojumi"/></div>
                </div>
            </div>
        </div>

</section>
<!-- Section -- about -->
<section class="section">

        <div class="section__content section__content--fluid-width section__content--about">
            <div class="grid grid--5col grid--about">


                <div class="grid__item grid__item--x2 grid__item--floated-right">
                    <h3 class="grid__title">Kā strādā SPARK <span>grāmatvedības pakalpojumi?</span></h3>
                    <p class="grid__text">Izmanto mūsu biznesa vadības risinājumu, kas ir iekļauts pakalpojuma cenā. Tā <a href="https://smartbooks.lv/" target="_blank"><b>mākslīgais intelekts</b></a> veiks visus standarta operāciju grāmatojumus un mēs parūpēsimies, lai Tava uzņēmuma grāmatvedība ir pareiza, precīza un vienmēr ir gatava VID auditam.</p>
                    <ul class="grid__list">
                        <li>Reizi mēnesī pārbaudīsim Tava uzņēmuma darījumus un veiksim labojumus, ja tas būs nepieciešams</li>
                        <li>Pareizi aprēķināsim algas, nodokļus, iesniegsim laikā atskaites un runāsim ar VID Tavā vietā</li>
                        <li>Atbildēsim uz visiem Taviem jautājumiem par grāmatvedību un nodokļiem, kā arī iedosim viegli pielāgojamus dokumentu paraugus</li>
                    </ul>

                </div>
                <div class="grid__item grid__item--x3">
                    <div class="grid__image grid__image--left" data-paroller-factor="0.2" data-paroller-type="foreground" data-paroller-direction="vertical"><img src="images/desktop-frame-about-2.png" alt="Grāmatvedības pakalpojumi ar mākslīgā intelekta atbalstu." title="Tiešsaistes grāmatvedības pakalpojumi"/></div>
                </div>
            </div>
        </div>

</section>
<!-- Section -- about -->
<section class="section" id="about2">

        <div class="section__content section__content--fluid-width section__content--about">
            <div class="grid grid--5col grid--about">

                <div class="grid__item grid__item--x2">
                    <h3 class="grid__title">Ko SPARK dos <span>Tavam uzņēmumam?</span></h3>
                    <p class="grid__text">Piekļuve intelektuālajam kapitālam var būt dārga. Īpaši maziem un jaunizveidotiem uzņēmumiem. Ar SPARK Tu iegūsti <a href="https://confidentum.lv/" target="_blank"><b>profesionālus un ISO 9001:2015 sertificētus</b></a> grāmatvedības pakalpojumus, kurus, iespējams, savādāk nevarētu atļauties. Fiksēta pakalpojuma maksa ļauj precīzi plānot un kontrolēt grāmatvedības izmaksas. Šī ir mūsdienīga un uz izaugsmi vērsta pieeja, kas Tev palīdzēs virzīties uz priekšu.</p>
                    <ul class="grid__list">
                        <li>Piekļūsti intelektuālajam kapitālam</li>
                        <li>Fiksētas grāmatvedības izmaksas</li>
                        <li>Attīsti uzņēmumu ar mūsdienīgu un uz izaugsmi orientētu pieeju</li>
                    </ul>

                </div>
                <div class="grid__item grid__item--x3">
                    <div class="grid__image grid__image--right" data-paroller-factor="0.2" data-paroller-type="foreground" data-paroller-direction="vertical"><img src="images/desktop-frame-about-3.png" alt="Confidentum SPARK ir grāmatvedības pakalpojumi par fiksētu maksu, kas nav atkarīga no operāciju skaita." title="Grāmatvedības pakalpojumi par fiksētu maksu"/></div>
                </div>
            </div>
        </div>

</section>

<!-- Section -->
<section class="section section--clients" id="clients" style="padding-top: 90px; margin-top: -90px">
        <div class="section__content section__content--fluid-width">
            <div class="grid grid--5col">

                <div class="grid__item">
                    <div class="grid__client-logo"><a href="https://www.bureauveritas.lv/" target="_blank"><img src="images/clients/BV_Certification_ISO9001.gif" alt="Bureau Veritas" title="Bureau Veritas"/></a></div>
                </div>
                <div class="grid__item">
                    <div class="grid__client-logo"><a href="http://www.lrga.lv/" target="_blank"><img src="images/clients/lrga.png" alt="Latvijas Republikas Grāmatvežu Asociācija" title="LRGA"/></a></div>
                </div>
                <div class="grid__item">
                    <div class="grid__client-logo"><a href="https://www.chamber.lv/" target="_blank"><img src="images/clients/ltrk.png" alt="Latvijas Tirdzniecības un Rūpniecības Kamera" title="LTRK"/></a></div>
                </div>
                <div class="grid__item">
                    <div class="grid__client-logo"><a href="https://www.intrum.lv/biznesa-risinajumi/" target="_blank"><img src="images/clients/intrum.svg" alt="Intrum" title="Intrum"/></a></div>
                </div>
                <div class="grid__item">
                    <div class="grid__client-logo"><a href="https://www.capitalia.com/lv/" target="_blank"><img src="images/clients/capitalia.png" alt="Capitalia" title="Capitalia"/></a></div>
                </div>

            </div>

        </div>
        <svg class="svg-cta-top" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none">
            <path d="M0,70 C30,130 70,50 100,70 L100,100 0,100 Z" fill="#27ada6"/>
        </svg>
</section>

<section class="section section--cta" id="cta">
        <div class="section__content section__content--fluid-width section__content--padding section__content--cta">
                <h2 class="section__title section__title--centered section__title--cta">Vai meklē grāmatvedības ekspertu?</h2>
            <div class="section__description section__description--centered section__description--cta">
            Mēs ietaupīsim Tev stundām stresa un darba katru mēnesi. Pieslēdz SPARK tagad un kļūsim par Tavu labāko sadarbības partneri!
            </div>
            <div class="intro__buttons intro__buttons--centered">
            <a class="btn btn--orange-bg" href="#pricing">SĀKT TAGAD</a>
            </div>
        </div>
        <svg class="svg-cta-bottom" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none">
            <path d="M0,70 C30,130 70,50 100,70 L100,100 0,100 Z" fill="#ffffff"/>
        </svg>
</section>

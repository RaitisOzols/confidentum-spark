<section class="section" id="faq">
    <div class="section__content section__content--fluid-width section__content--padding">
        <div class="grid grid--5col grid--faq">
            <div class="grid__item grid__item--x2">

                <h3 class="grid__title">Visbiežāk uzdotie jautājumi</h3>
                <p class="grid__text">Vai Tev ir jautājumi par grāmatvedības procesu vai pakalpojuma uzsākšanu? Tad Tu esat īstajā vietā!</p>
                <ul class="grid__list">
                    <li>Uzņēmumi, kas ir atvērti jaunām grāmatvedības tehnoloģijām, divreiz biežāk uzrāda lielāku peļņu nekā tie, kuri to nedara.</li>
                    <!-- <li>Confidentum SPARK - sertificēti grāmatveži, profesionālā apdrošināšana.</li> -->
                </ul>

            </div>
            <div class="grid__item grid__item--x3">
                <div class="faq">
                    <div class="faq__item">
                      <input class="faq__input" id="faq1" type="radio" name="faq" checked>
                      <label class="faq__label" for="faq1">Kāds ir pakalpojuma uzsākšanas process? <span></span></label>
                      <div class="faq__content">
                        <p>Pēc pakalpojuma pieslēgšanas, uz Tavu e-pastu tiks nosūtītas instrukcijas ar turpmākajām darībām, Tava grāmatveža kontakti un saite uz biznesa vadības programmu.</p>
                      </div>
                    </div>
                    <div class="faq__item">
                      <input class="faq__input" id="faq2" type="radio" name="faq">
                      <label class="faq__label" for="faq2">Vai operāciju skaits ir ierobežots?<span></span></label>
                      <div class="faq__content">
                        <p>Operāciju skaits ir neierobežots. Izmantojot Confindentum SPARK grāmatvedības pakalpojumus cena ir fiksēta un nav atkarīga no rēķinu, bankas un citu operāciju skaita.</p>
                      </div>
                    </div>
                    <div class="faq__item">
                      <input class="faq__input" id="faq3" type="radio" name="faq">
                      <label class="faq__label" for="faq3">Kā varu būt pārliecināts par grāmatvedības pareizību? <span></span></label>
                      <div class="faq__content">
                        <p>Confidentum SPARK komandā ir profesionāli grāmatveži un mūsu kvalitātes vadības sistēma ir sertificēta atbilstši <a href="https://www.bureauveritas.lv/services+sheet/iso-9001-sertifikacija" target="
                            "><b>ISO 9001:2015</b></a> standartam. Esam arī Latvijas Republikas Grāmatvežu asociācijas biedri.</p>
                      </div>
                    </div>
                    <div class="faq__item">
                      <input class="faq__input" id="faq4" type="radio" name="faq">
                      <label class="faq__label" for="faq4">Kā garantējat kvalitāti un vai uzņematies atbildību? <span></span></label>
                      <div class="faq__content">
                        <p>Mūsu pakalpojumi ir civiltiesiski apdrošināti par 50 000 EUR.</p>
                      </div>
                    </div>
                    <div class="faq__item">
                      <input class="faq__input" id="faq5" type="radio" name="faq">
                      <label class="faq__label" for="faq5">Kas notiek VID audita gadījumā? <span></span></label>
                      <div class="faq__content">
                        <p>Mēs satavosim visus VID pieprasītos grāmatvedības pārskatus. Ja vēlies, lai Tavs grāmatvedis arī fiziski piedalās auditā vari iegādāties mūsu papildus pakalpojumu <a href="/pricelist"><b>VID Kasko</b></a>.</p>
                      </div>
                    </div>

                    <div class="faq__item">
                      <input class="faq__input" id="faq6" type="radio" name="faq">
                      <label class="faq__label" for="faq6">Kā varu mainīt pakalpojumu plānu?<span></span></label>
                      <div class="faq__content">
                        <p>Vari mainīt pakalpojumu plānu jebkurā laikā pieslēdzoties savam kontam. Jau samaksātā nauda par atlikušo periodu tiks ņemta vērā kā priekšapmaksa un Tev būs jāpiemaksā tikai starpība.</p>
                      </div>
                    </div>

                    <!-- <div class="faq__item">
                      <input class="faq__input" id="faq7" type="radio" name="faq">
                      <label class="faq__label" for="faq7">Vai varat palīdzēt ar parādu piedziņu? <span></span></label>
                      <div class="faq__content">
                        <p>Jā! Sadarbojoties ar vadošo Eiropas kredītu pārvaldības uzņēmumu <a href="https://www.intrum.lv/saistibu-parvaldisana/" target="_blank"><b>Intrum</b></a> saviem klientiem varam sniegt operatīvu un profesionālu palīdzību parādu atgūšanā.</p>
                      </div>
                    </div>

                    <div class="faq__item">
                      <input class="faq__input" id="faq8" type="radio" name="faq">
                      <label class="faq__label" for="faq8">Kā iegūt finansējumu uzņēmuma attīstībai? <span></span></label>
                      <div class="faq__content">
                        <p>Sadarbojoties ar vadošo mazo un vidējo uzņēmumu finansētāju Baltijas valstīs <a href="https://www.capitalia.com/lv" target="_blank"><b>Capitalia</b></a> saviem klientiem varam atvieglot finansējuma iegūšanas procesu.</p>
                      </div>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</section>

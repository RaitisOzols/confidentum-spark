<section class="section section--features" id="features">

        <div class="section__content section__content--fluid-width section__content--features">
                <h1 class="section__title section__title--centered">Grāmatvedības pakalpojumi Tavam uzņēmumam</h1>
            <div class="section__description section__description--centered">
            Mēs pārņemsim grāmatvedības kārtošanu, nodokļu aprēķināšanu, gada un mēneša pārskatu sastādīšanu un visu komunikāciju ar VID!
            Labākā daļa? Viss tiks izdarīts precīzi, izmantojot mūsu
            <br>
            <a href="https://smartbooks.lv/" target="_blank"><b>jauno biznesa vadības risinājumu ar mākslīgo intelektu</b></a>.
            <br>
            Tu esi pelnījis vairāk nekā nekvalificētu palīdzību un negaidītas izmaksas. Lūk, kā mēs padarām grāmatvedības pakalpojumus labākus nekā konkurenti:
            </div>
            <div class="grid grid--3col grid--features">

                <div class="grid__item">
                    <div class="grid__icon"><img src="images/icons/icons-64-green/security-64.png" alt="Grāmatvedības pakalpojumi un profesionāla grāmatveža atbalsts par fiksētu cenu." title="Sertificēti grāmatvedības pakalpojumi"/></div>
                    <h3 class="grid__title">Vienkārši un bez stresa</span> </h3>
                    <p class="grid__text">Iegūstiet stabilu un modernu <a href="https://smartbooks.lv/" target="_blank">biznesa vadības risinājumu</a> un zinošu grāmatvedības ekspertu atbalstu par <b>fiksētu cenu</b>, lai Tava grāmatvedība vienmēr būtu pilnīgā kārtībā.</p>
                </div>

                <div class="grid__item">
                    <div class="grid__icon"><img src="images/icons/icons-64-green/target-64.png" alt="Ietaupi līdz 10 stundām vai vairāk laika katru mēnesi ar Confidentum SPARK grāmatvedības pakalpojumiem." title="Izdevīgi grāmatvedības pakalpojumi"/></div>
                    <h3 class="grid__title">Koncentrējies uz attīstību</span></h3>
                    <p class="grid__text">Grāmatvedības kārtošana var aizņemt līdz <span>10 stundām (vai vairāk)</span> katru mēnesi. Parūpējieties par to, kas Tev ir patiešām svarīgs, savukārt mēs parūpēsimies par obligāto grāmatvedības prasību izpildi.</p>
                </div>

                <div class="grid__item">
                    <div class="grid__icon"><img src="images/icons/icons-64-green/desktop-chart-64.png" alt="Confidentum SPARK grāmatvedības pakalpojumi ļaus Tev pilnībā kontrolēt uzņēmuma finanses." title="Confidentum SPARK grāmatvedības pakalpojumi"/></div>
                    <h3 class="grid__title">Dati bez kā nevar iztikt</h3>
                    <p class="grid__text">Automātiskas atskaites ļaus Tev pilnībā kontrolēt uzņēmuma finanses. Acumirklī saskati kopainu un redzi praktiskas iespējas, kā <b>attīstīt Tavu uzņēmumu</b>. Tu nekad vairs nebūsi tumsā!</p>
                </div>

            </div>
        </div>


</section>

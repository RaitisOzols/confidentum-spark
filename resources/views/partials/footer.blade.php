<footer class="footer" id="footer">
        <div class="footer__content footer__content--fluid-width footer__content--svg">

            <div class="grid grid--5col">

                <div class="grid__item grid__item--x2">
                    <h3 class="grid__title grid__title--footer-logo">Confidentum SPARK</h3>
                    <p class="grid__text grid__text--copyright">Autortiesības &copy; 2019 Confidentum.  <br />Visas tiesības aizsargātas. Ar lepnumu no Latvijas. </p>
                    <ul class="grid__list grid__list--sicons">
                            <li><a href="#"><img src="images/social/black/twitter.png" alt="Confidentum SPARK grāmatvedības pakalpojumi Twitter lapa" title="Confidentum SPARK Twitter lapa"/></a></li>
                            <li><a href="https://www.facebook.com/confidentumspark" target="_blank"><img src="images/social/black/facebook.png" alt="Confidentum SPARK grāmatvedības pakalpojumi Facebook lapa" title="Confidentum SPARK Facebook"/></a></li>
                            <li><a href="https://www.linkedin.com/showcase/confidentum-spark" target="_blank"><img src="images/social/black/linkedin.png" alt="Confidentum SPARK grāmatvedības pakalpojumi LinkedIn lapa" title="Confidentum SPARK LinkedIn lapa"/></a></li>
                    </ul>
                </div>
                <div class="grid__item">
                    <h3 class="grid__title grid__title--footer">Uzņēmums</h3>
                    <ul class="grid__list grid__list--fmenu">
                            <li><a href="https://confidentum.lv" target="_blank">Par Mums</a></li>
                            <li><a href="/quality" target="_blank">Kvalitātes Politika</a></li>
                            <!-- <li><a href="#">Karjera</a></li> -->
                            <!-- <li><a href="#">Dalība Organizācijās</a></li> -->
                            <!-- <li><a href="#">Sadarbības Programma</a></li> -->
                            <!-- <li><a href="#">Atrašanās Vieta</a></li> -->
                    </ul>
                </div>
                <div class="grid__item">
                    <h3 class="grid__title grid__title--footer">Pakalpojumi</h3>
                    <ul class="grid__list grid__list--fmenu">
                            <li><a href="/pricelist" target="_blank">Cenu Lapa</a></li>
                            <li><a href="/contract" target="_blank">Lietošanas Noteikumi</a></li>
                            <li><a href="/privacy" target="_blank">Privātuma Politika</a></li>
                            <li><a href="/vdar" target="_blank">Datu Apstrāde</a></li>

                    </ul>
                </div>
                <div class="grid__item">
                    <h3 class="grid__title grid__title--footer">Kontakti</h3>
                    <ul class="grid__list grid__list--fmenu">
                            <li><a>spark<code>@</code>confidentum.lv</a></li>
                            <li><a>+371 67035353</a></li>
                            <li><a>Krišjāņa Valdemāra 21, Rīga, LV-1010</a></li>
                            <!-- <li><a href="#">Kontakti</a></li> -->
                            <!-- <li><a href="#">BUJ</a></li> -->
                            <!-- <li><a href="#">Video</a></li> -->
                            <!-- <li><a href="#">Rokasgrāmata</a></li> -->
                    </ul>
                </div>

            </div>


        </div>


</footer>

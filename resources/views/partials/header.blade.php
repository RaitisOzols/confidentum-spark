<header class="header">
    <div class="header__content header__content--fluid-width">
        <div class="header__logo-title">
            <a href="/" style="color: inherit">
                <span style="font-size: 0.75em">Confidentum</span>
                SPARK
            </a>
        </div>
        <nav class="header__menu">
            <ul>
                <li><a href="#intro" class="selected header-link">SĀKUMS</a></li>
                <li class="menu-item-has-children"><a href="#features" class="header-link">SADAĻAS</a>
                    <ul class="sub-menu">
                        <li><a href="#features" class="header-link">MŪSU PAKALPOJUMI</a></li>
                        <li><a href="#about" class="header-link">KĀ TAS STRĀDĀ</a></li>
                        <!-- <li><a href="#testimonials" class="header-link">ATSAUKSMES</a></li> -->

                        <li><a href="#faq" class="header-link">ATBALSTS</a></li>
                        <li><a href="#clients" class="header-link">MŪSU PARTNERI</a></li>

                    </ul>
                </li>
                <li><a href="#pricing" class="header-link">CENAS</a></li>
                {{-- <li><a href="/blog" class="header-link">AKADĒMIJA</a></li> --}}
                <li><a href="#support" class="header-link">KONTAKTI</a></li>
                <li class="header__btn header__btn--login"><a href="javascript:void(0)" data-cb-type="portal" >MANS SPARK</a></li>
                <li class="header__btn header__btn--signup"><a href="#pricing">GATAVS STARTAM</a></li>
            </ul>
        </nav>
    </div>
</header>

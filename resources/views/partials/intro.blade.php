<section class="section section--intro" id="intro">
        <div class="section__content section__content--fluid-width section__content--intro">
            <div class="intro">
                <div class="intro__content">
                <h1 class="intro__title"><span>Grāmatvedības pakalpojumi</span></h1>
                <h2 class="intro__subtitle" style="font-weight: normal">Tiešsaistes grāmatvedības pakalpojumi jauniem un augošiem uzņēmumiem.</h2>
                <h3  style="font-weight: normal" class="intro__description">Izdevīgas cenas sākot no <span>€99</span> mēnesī</h3>
                <div class="intro__buttons intro__buttons--left">

                    <a class="btn btn--blue-bg modal-toggle" href="" data-openpopup="signuplogin" data-popup="support-form">
                        <span class="hide-sm">PIETEIKT KONSULTĀCIJU</span>
                        <span class="show-sm">KONSULTĀCIJA</span>
                    </a>

                    <a class="btn btn--orange-bg" href="#pricing">
                        <span class="hide-sm">&nbsp;&nbsp;&nbsp;SĀKT TAGAD&nbsp;&nbsp;&nbsp;</span>
                        <span class="show-sm">SĀKT TAGAD</span>
                    </a>
                </div>
                </div>
            </div>
        </div>
        <div class="intro-animation">
            <img src="images/intro-animation.png" alt="Sertificēti tiešsaistes grāmatvedības pakalpojumi par fiksētu cenu bez operāciju skaita ierobežojuma." title="Confidentum SPARK grāmatvedības pakalpojumi maziem un vidējiem uzņēmumiem"/>
        </div>
        <svg class="svg-intro-bottom" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none">
            <path d="M95,0 Q90,90 0,100 L100,100 100,0 Z" fill="#ffffff"/>
        </svg>
</section>

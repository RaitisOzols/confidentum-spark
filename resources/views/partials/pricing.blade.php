<!-- Section -- pricing -->
<section class="section" id="pricing">

        <div class="section__content section__content--fluid-width section__content--padding">
                <h2 class="section__title section__title--centered">Draudzīgas grāmatvedības pakalpojumu cenas</h2>
            <div class="section__description section__description--centered">
            Izvēlies grāmatvedības pakalpojumu plānu, kas ir piemērots Tev. Nekādu līgumu. Atcel jebkurā laikā.
            <br>
            Trīs minūtes un Tev vairs nekad nevajadzēs uztraukties ar grāmatvedību.
            </div>


                <div class="pricing">
                    <div class="pricing__switcher switcher">
                        <div class="switcher__buttons">
                            <div class="switcher__button switcher__button--enabled">Gada cenas</div>
                            <div class="switcher__button">Mēneša cenas</div>
                            <div class="switcher__border"></div>
                        </div>

                    </div>


                    <div class="pricing__plan">
                        <h3 class="pricing__title">STARTS</h3>
                        <div class="pricing__values">
                            <div class="pricing__value pricing__value--show"><span>€</span>89 <b>/ mēnesī</b></div>
                            <div class="pricing__value pricing__value--hide pricing__value--hidden"><span>€</span>99 <b>/ mēnesī</b></div>
                        </div>
                        <ul class="pricing__list">

                            <li><b>1 Darbinieks*</b></li>
                            <li><b>5 Lielāko rēķinu pārbaude</b></li>
                            <li><b>15 Minūtes konsultācijas</b></li>
                            <li><b>Pamata Biznesa Inteliģence</b></li>
                            <li>PVN, UIN un algu atskaites VID</li>
                            <li><a href="https://smartbooks.lv/" target="_blank">Biznesa vadības programma</a></li>
                            <li>Dokumentu paraugi</li>
                            <li class="disabled">Personīgs grāmatvedis</li>
                            <li class="disabled">Prioritāra dokumentu apstrāde</li>
                            <li class="disabled">Riska analīze un ieteikumi</li>
                            <li class="disabled">Individuāli uzskaites risinājumi</li>
                            <li class="disabled">Dokumentu ievade un arhīvs</li>
                        </ul>
                        <a class="pricing__signup pricing__toggle" href="javascript:void(0)" data-cb-type="checkout" data-cb-plan-id="starts-yearly" >PIESLĒGT TAGAD</a>
                        <a class="pricing__signup pricing__toggle pricing__signup--hidden" href="javascript:void(0)" data-cb-type="checkout" data-cb-plan-id="starts-monthly" >PIESLĒGT TAGAD</a>

                    </div>
                    <div class="pricing__plan pricing__plan--popular">
                             <div class="pricing__badge-bg"></div>
                         <div class="pricing__badge-text">POPULĀRS</div>
                        <h3 class="pricing__title">PRO</h3>
                        <div class="pricing__values">
                            <div class="pricing__value pricing__value--show"><span>€</span>269 <b>/ mēnesī</b></div>
                            <div class="pricing__value pricing__value--hide pricing__value--hidden"><span>€</span>299 <b>/ mēnesī</b></div>
                        </div>
                        <ul class="pricing__list">

                            <li><b>10 Darbinieki*</b></li>
                            <li><b>20 Lielāko rēķinu pārbaude</b></li>
                            <li><b>30 Minūtes konsultācijas</b></li>
                            <li><b>Pamata Biznesa Inteliģence</b></li>
                            <li>PVN, UIN un algu atskaites VID</li>
                            <li><a href="https://smartbooks.lv/" target="_blank">Biznesa vadības programma</a></li>
                            <li>Dokumentu paraugi</li>
                            <li>Personīgs grāmatvedis</li>
                            <li>Prioritāra dokumentu apstrāde</li>
                            <li>Riska analīze un ieteikumi</li>
                            <li class="disabled">Individuāli uzskaites risinājumi</li>
                            <li class="disabled">Dokumentu ievade un arhīvs</li>
                        </ul>
                        <a class="pricing__signup pricing__toggle" href="javascript:void(0)" data-cb-type="checkout" data-cb-plan-id="pro-yearly" >PIESLĒGT TAGAD</a>
                        <a class="pricing__signup pricing__toggle pricing__signup--hidden" href="javascript:void(0)" data-cb-type="checkout" data-cb-plan-id="pro-monthly" >PIESLĒGT TAGAD</a>
                    </div>
                    <div class="pricing__plan">
                        <h3 class="pricing__title">INDIVIDUĀLAIS</h3>
                        <div class="pricing__values">
                            <div class="pricing__value pricing__value--show"><span>no €</span>829 <b>/ mēnesī</b></div>
                            <div class="pricing__value pricing__value--hide pricing__value--hidden"><span>no €</span>899 <b>/ mēnesī</b></div>
                        </div>
                        <ul class="pricing__list">

                            <li><b>Neierobežoti darbinieki</b> </li>
                            <li><b>Neierobežota rēķinu pārbaude</b></li>
                            <li><b>Neierobežotas konsultācijas</b></li>
                            <li><b>Pielāgota Biznesa Inteliģence</b></li>
                            <li>PVN, UIN un algu atskaites VID</li>
                            <li><a href="https://smartbooks.lv/" target="_blank">Biznesa vadības programma</a></li>
                            <li>Dokumentu paraugi</li>
                            <li>Personīgs grāmatvedis</li>
                            <li>Prioritāra dokumentu apstrāde</li>
                            <li>Riska analīze un ieteikumi</li>
                            <li>Individuāli uzskaites risinājumi</li>
                            <li>Dokumentu ievade un arhīvs</li>
                        </ul>
                        <a class="pricing__signup modal-toggle" href="" data-openpopup="signuplogin" data-popup="contact-form">PIESLĒGT TAGAD</a>
                    </div>
                </div>

            <div class="clear">
                <div id="price_note">
                    <p style="font-size: 110%">
                        *Ja darbinieku ir vairāk nekā iekļauts izvēlētajā grāmatvedības pakalpojumu plānā, pieslēdz <a href="/pricelist"><b>papildus darbinieku </b></a>algu aprēķina paku. <br>
                        Papildus pakalpojumu saraksts un pilna cenu lapa atrodama <a href="/pricelist"><b>šeit</b></a>. Cenas ir norādītas bez pvn.
                    </p>
                </div>
            </div>
        </div>

</section>

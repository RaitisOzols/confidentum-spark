<section class="section" id="support" style="margin-top: -100px">
        <div class="section__content section__content--fluid-width section__content--padding">
            <div class="grid grid--2col grid--support">
                <div class="grid__item grid__item--padding">
                    <h3 class="grid__title">Palīdzība &amp; Atbalsts</h3>
                    <h5>spark<code>@</code>confidentum.lv | 67035353</h5>
                    <p class="grid__text">Tava uzņēmuma izaicinājumi ir mūsu galvenā prioritāte. Mūsu atbalsta komanda ir šeit, lai palīdzētu. Vai Tev ir jautājums par mūsu pakalpojumu? Vai arī tehniska problēma? Mēs labprāt to uzklausītu!</p>
                </div>
                <div class="grid__item grid__item--padding grid__item--centering">
                    <a class="grid__more modal-toggle" href="" data-openpopup="signuplogin" data-popup="support-form">
                        <span class="hide-sm">PIETEIKT KONSULTĀCIJU</span>
                        <span class="show-sm">KONSULTĀCIJA</span>
                    </a>
                </div>
                <svg class="svg-support-bottom" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none">
                <path d="M0,90 Q0,100 3,100 L95,96 Q100,96 99,80 L95,25 Q94,15 90,15 L6,0 Q2,0 2,10 Z" fill="#ffba00"/>
                </svg>
            </div>
            <div class="clear"></div>
        </div>

</section>

<!-- Section -- testimonials -->
<section class="section section--testimonials" id="testimonials">

        <div class="section__content section__content--fluid-width section__content--padding">
                <h2 class="section__title section__title--centered">Veiksmes stāsti</h2>
                  <div class="testimonials">
                    <div class="testimonials__content swiper-wrapper">
                          <div class="testimonials__slide swiper-slide">
                            <div class="testimonials__thumb" data-swiper-parallax="-50%"><img src="images/avatar-1.jpg" alt="" title=""/></div>
                            <div class="testimonials__source">Lason Duvan <a href="#">New York Business Center</a></div>
                            <div class="testimonials__text" data-swiper-parallax="-100%"><p>"Owning a small business and doing the books at the
same time can take your focus away from growing the
company. Having help from accounting professionals
that understand your business and accounting
procedures could be the best thing an entrepreneur
can have."</p></div>

                          </div>
                          <div class="testimonials__slide swiper-slide">
                            <div class="testimonials__thumb" data-swiper-parallax="-50%"><img src="images/avatar-2.jpg" alt="" title=""/></div>
                            <div class="testimonials__source">Jada Sacks <a href="#">Paris Tehnics</a></div>
                            <div class="testimonials__text" data-swiper-parallax="-100%"><p>"My accountant is very responsive to my
questions and provides answers in light of my
business model and goals.”"</p></div>

                          </div>
                          <div class="testimonials__slide swiper-slide">
                            <div class="testimonials__thumb" data-swiper-parallax="-50%"><img src="images/avatar-3.jpg" alt="" title=""/></div>
                            <div class="testimonials__source">Lason Duvan <a href="#">Music Software</a></div>
                            <div class="testimonials__text" data-swiper-parallax="-100%"><p>"My accounting firm has exceptional customer
service, transparency and ability to clearly
communicate my business needs."</p></div>

                          </div>
                          <div class="testimonials__slide swiper-slide">
                            <div class="testimonials__thumb" data-swiper-parallax="-50%"><img src="images/avatar-4.jpg" alt="" title=""/></div>
                            <div class="testimonials__source">Duran Jackson <a href="#">New York Business Center</a></div>
                            <div class="testimonials__text" data-swiper-parallax="-100%"><p>"Generally, every customer wants a product or service that solves their problem, worth their money"</p></div>

                          </div>
                          <div class="testimonials__slide swiper-slide">
                            <div class="testimonials__thumb" data-swiper-parallax="-50%"><img src="images/avatar-5.jpg" alt="" title=""/></div>
                            <div class="testimonials__source">Maria Allesi <a href="#">Italy Solutions</a></div>
                            <div class="testimonials__text" data-swiper-parallax="-100%"><p>"No one can make you successful; the will to success comes from within.' I've made this my motto."</p></div>

                          </div>
                          <div class="testimonials__slide swiper-slide">
                            <div class="testimonials__thumb" data-swiper-parallax="-50%"><img src="images/avatar-6.jpg" alt="" title=""/></div>
                            <div class="testimonials__source">Jenifer Patrison<a href="#">App Dating</a></div>
                            <div class="testimonials__text" data-swiper-parallax="-100%"><p>"Can change their circumstances and rise as high as they are willing to work"</p></div>

                          </div>
                    </div>

                    <div class="testimonials__pagination swiper-pagination"></div>
                            <div class="testimonials__button--next swiper-button-next"></div>
                        <div class="testimonials__button--prev swiper-button-prev"></div>
                </div>
            <div class="clear"></div>
        </div>

</section>

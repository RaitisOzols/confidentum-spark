<table id="bundles">
    <thead>
        <tr>
            <th>Grāmatvedības pakalpojumu komplekti</th>
            <th width="15%">STARTS</th>
            <th width="15%">PRO</th>
            <th width="15%">INDIVIDUĀLAIS</th>
        </tr>
        <tr>
            <th>Cena mēnesī bez pvn</th>
            <th>99</th>
            <th>299</th>
            <th>no 899</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Darbinieki</td>
            <td>1</td>
            <td>10</td>
            <td>Neierobežoti</td>
        </tr>
        <tr>
            <td>Lielāko rēķinu pārbaude</td>
            <td>5</td>
            <td>20</td>
            <td>Neierobežoti</td>
        </tr>
        <tr>
            <td>Konsultācijas, minūtes</td>
            <td>15</td>
            <td>30</td>
            <td>Neierobežoti</td>
        </tr>
        <tr>
            <td>Personīgs grāmatvedis</td>
            <td>+</td>
            <td>+</td>
            <td>+</td>
        </tr>
        <tr>
            <td>PVN, UIN un algu atskaites VID</td>
            <td>+</td>
            <td>+</td>
            <td>+</td>
        </tr>
        <tr>
            <td>Biznesa vadības programma</td>
            <td>+</td>
            <td>+</td>
            <td>+</td>
        </tr>
        <tr>
            <td>Dokumentu paraugi</td>
            <td>+</td>
            <td>+</td>
            <td>+</td>
        </tr>
        <tr>
            <td>Prioritāra dokumentu apstrāde</td>
            <td></td>
            <td>+</td>
            <td>+</td>
        </tr>
        <tr>
            <td>Riska analīze un ieteikumi</td>
            <td></td>
            <td>+</td>
            <td>+</td>
        </tr>
        <tr>
            <td>Individuāli uzskaites risinājumi</td>
            <td></td>
            <td></td>
            <td>+</td>
        </tr>
        <tr>
            <td>Dokumentu ievade un arhīvs</td>
            <td></td>
            <td></td>
            <td>+</td>
        </tr>
        <tr>
            <td></td>
            <td>
                <a data-cb-type="checkout" data-cb-plan-id="starts-monthly" href="javascript:void(0)" class="btn--cart btn--blue-border">
                    <i class="fa fa-cart-arrow-down fa-lg" aria-hidden="true"></i>
                </a>
            </td>
            <td>
                <a data-cb-type="checkout" data-cb-plan-id="pro-monthly" href="javascript:void(0)" class="btn--cart btn--blue-border">
                    <i class="fa fa-cart-arrow-down fa-lg" aria-hidden="true"></i>
                </a>
            </td>
            <td>
                <a href="" data-openpopup="signuplogin" data-popup="contact-form" class="modal-toggle btn--cart btn--blue-border">
                    <i class="fa fa-cart-arrow-down fa-lg" aria-hidden="true"></i>
                </a>
            </td>
        </tr>
    </tbody>
</table>

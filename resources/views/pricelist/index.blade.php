<!DOCTYPE html>
<html lang="lv" dir="ltr">
<head>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-154035955-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-154035955-1');
</script>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<meta name="author" content="Confidentum" />
<meta name="description" content="Confidentum SPARK grāmatvedības pakalpojumu cenas" />
<meta name="keywords" content="Grāmatvedības pakalpojumu cenas" />
<title>Confidentum Spark</title>
<link rel="stylesheet" href="css/swiper.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/custom.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
<link rel="manifest" href="/favicon/site.webmanifest">

<style media="screen">
    table {
        border-collapse: collapse;
        width: 100%;
        margin-top: 5vh;
    }

    table th {
        background-color: rgb(0, 94, 89);
        color: #fff;
    }

    table tr:hover {
        background-color: #f7f8f9;
    }

    table, th, td {
        border: 1px solid black;
        padding: 10px;
    }

    table tbody tr td:nth-child(2) {
        text-align: center;
    }

    #bundles tbody tr td:nth-child(3), #bundles tbody tr td:nth-child(4) {
        text-align: center;
    }

    .shopping-cart tbody tr td:nth-child(3) {
        padding-top: 0px;
        padding-bottom: 0px;
        text-align: center;
    }

    .btn--cart {
        display: inline-block;
        width: 70%;
        font-size: 100%;
        padding-top: 3px;
        padding-bottom: 3px;

       -webkit-border-radius: 25px;
       -moz-border-radius: 25px;
       border-radius: 25px;
       -webkit-transition: all 0.3s ease;
       -moz-transition: all 0.3s ease;
       transition: all 0.3s ease;
    }

</style>


<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,900" rel="stylesheet">

<script src="https://js.chargebee.com/v2/chargebee.js" data-cb-site="confidentum-spark-test" ></script>

<!-- Calendly link widget begin -->
<link href="https://assets.calendly.com/assets/external/widget.css" rel="stylesheet">
<script src="https://assets.calendly.com/assets/external/widget.js" type="text/javascript"></script>
<!-- Calendly link widget end -->

</head>
<body>

    <header class="header">
        <div class="header__content header__content--fluid-width">
            <div class="header__logo-title">
                <a href="/" style="color: inherit">
                    <span style="font-size: 0.75em">Confidentum</span>
                    SPARK
                </a>
            </div>
            <nav class="header__menu">
                <ul>
                    <li><a href="/#intro" class="selected header-link">SĀKUMS</a></li>
                    <li class="menu-item-has-children"><a href="/#features" class="header-link">SADAĻAS</a>
                        <ul class="sub-menu">
                            <li><a href="/#features" class="header-link">MŪSU PAKALPOJUMI</a></li>
                            <li><a href="/#about" class="header-link">KĀ TAS STRĀDĀ</a></li>
                            <!-- <li><a href="#testimonials" class="header-link">ATSAUKSMES</a></li> -->

                            <li><a href="/#faq" class="header-link">ATBALSTS</a></li>
                            <li><a href="/#clients" class="header-link">MŪSU PARTNERI</a></li>

                        </ul>
                    </li>
                    <li><a href="/#pricing" class="header-link">CENAS</a></li>
                    {{-- <li><a href="/blog" class="header-link">AKADĒMIJA</a></li> --}}
                    <li><a href="/#support" class="header-link">KONTAKTI</a></li>
                    <li class="header__btn header__btn--login"><a href="javascript:void(0)" data-cb-type="portal" >MANS SPARK</a></li>
                    <li class="header__btn header__btn--signup"><a href="/#pricing">GATAVS STARTAM</a></li>
                </ul>
            </nav>
        </div>
    </header>

    <section class="section">

        <div class="section__content section__content--fluid-width section__content--padding">

            <div class="container">

                <h1>Grāmatvedības pakalpojumu cenas</h1>

                @include('pricelist.bundles')

                @include('pricelist.regular-addons')

                @include('pricelist.one-time-addons')

            </div>

        </div>

    </section>

    <section class="modal modal--signuplogin">
      <div class="modal__overlay modal__overlay--toggle"></div>
      <div class="modal__wrapper modal-transition">

      <div class="modal__body">

          @include('modals.support.contact-form')

      </div>

      </div>
    </section>    <!-- Modal for Login and Signup -->


<script src="../js/jquery-3.3.1.min.js"></script>
<script src="../js/jquery.paroller.min.js"></script>
<script src="../js/jquery.custom.js"></script>
<script src="../js/swiper.min.js"></script>
<script src="../js/swiper.custom.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
    	"use strict";
    	if (jQuery.isFunction(jQuery.fn.paroller) && screen.width > 800 ) {
    		$("[data-paroller-factor]").paroller();
    	}

        $('.header').addClass('header--sticky');
        $('.header__menu ul ul').addClass('submenu-header-sticky');
    });
</script>
<script src="../js/custom.js"></script>
@stack('js')

<!-- Start of HubSpot Embed Code -->
<script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/6934008.js"></script>
<!-- End of HubSpot Embed Code -->

</body>
</html>

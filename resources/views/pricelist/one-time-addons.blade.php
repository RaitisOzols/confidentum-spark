<table class="shopping-cart">
    <thead>
        <tr>
            <th>Vienreizējie grāmatvedības pakalpojumi</th>
            <th width="20%">Cena bez pvn</th>
            <th width="10%">Pieteikt</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Attālināta grāmatvedības konsultācija, 15 minūtes</td>
            <td>19</td>
            <td>
                <a href="" class="btn--cart btn--blue-border calendly">
                    <i class="fa fa-cart-arrow-down fa-lg" aria-hidden="true"></i>
                </a>
            </td>
        </tr>
        <tr>
            <td>Struktūrvienības reģistrācijas/likvidācijas iesnieguma sagatavošana</td>
            <td>19</td>
            <td>
                <a href="javascript:void(0)" data-cb-type="portal" class="btn--cart btn--blue-border">
                    <i class="fa fa-cart-arrow-down fa-lg" aria-hidden="true"></i>
                </a>
            </td>
        </tr>
        <tr>
            <td>Saimnieciskās darbības veidu pievienošanas/noņemšanas iesnieguma sagatavošana</td>
            <td>19</td>
            <td>
                <a href="javascript:void(0)" data-cb-type="portal" class="btn--cart btn--blue-border">
                    <i class="fa fa-cart-arrow-down fa-lg" aria-hidden="true"></i>
                </a>
            </td>
        </tr>
        <tr>
            <td>PVN reģistrācijas iesnieguma sagatavošana</td>
            <td>49</td>
            <td>
                <a href="javascript:void(0)" data-cb-type="portal" class="btn--cart btn--blue-border">
                    <i class="fa fa-cart-arrow-down fa-lg" aria-hidden="true"></i>
                </a>
            </td>
        </tr>
        <tr>
            <td>Pašnodarbināto izmaksu deklarēšana par gadu</td>
            <td>99</td>
            <td>
                <a href="javascript:void(0)" data-cb-type="portal" class="btn--cart btn--blue-border">
                    <i class="fa fa-cart-arrow-down fa-lg" aria-hidden="true"></i>
                </a>
            </td>
        </tr>
        <tr>
            <td>Gada pārskats STARTS plānam (bez revidenta)</td>
            <td>99</td>
            <td>
                <a href="javascript:void(0)" data-cb-type="portal" class="btn--cart btn--blue-border">
                    <i class="fa fa-cart-arrow-down fa-lg" aria-hidden="true"></i>
                </a>
            </td>
        </tr>
        <tr>
            <td>Gada pārskats PRO plānam (bez revidenta)</td>
            <td>299</td>
            <td>
                <a href="javascript:void(0)" data-cb-type="portal" class="btn--cart btn--blue-border">
                    <i class="fa fa-cart-arrow-down fa-lg" aria-hidden="true"></i>
                </a>
            </td>
        </tr>
        <tr>
            <td>Sākuma atlikumu pārnešana STARTS plānam</td>
            <td>99</td>
            <td>
                <a href="" data-openpopup="signuplogin" data-popup="contact-form" class="modal-toggle btn--cart btn--blue-border">
                    <i class="fa fa-cart-arrow-down fa-lg" aria-hidden="true"></i>
                </a>
            </td>
        </tr>
        <tr>
            <td>Sākuma atlikumu pārnešana PRO plānam</td>
            <td>299</td>
            <td>
                <a href="" data-openpopup="signuplogin" data-popup="contact-form" class="modal-toggle btn--cart btn--blue-border">
                    <i class="fa fa-cart-arrow-down fa-lg" aria-hidden="true"></i>
                </a>
            </td>
        </tr>
        <tr>
            <td>Atskaišu labošana (par katru atskaiti)</td>
            <td>no 19</td>
            <td>
                <a href="" data-openpopup="signuplogin" data-popup="contact-form" class="modal-toggle btn--cart btn--blue-border">
                    <i class="fa fa-cart-arrow-down fa-lg" aria-hidden="true"></i>
                </a>
            </td>
        </tr>
        <tr>
            <td>Personīga konsultācija vai apmācības birojā</td>
            <td>no 199</td>
            <td>
                <a href="" data-openpopup="signuplogin" data-popup="contact-form" class="modal-toggle btn--cart btn--blue-border">
                    <i class="fa fa-cart-arrow-down fa-lg" aria-hidden="true"></i>
                </a>
            </td>
        </tr>
        <tr>
            <td>VID Kasko uz vienu gadu</td>
            <td>no 249</td>
            <td>
                <a href="" data-openpopup="signuplogin" data-popup="contact-form" class="modal-toggle btn--cart btn--blue-border">
                    <i class="fa fa-cart-arrow-down fa-lg" aria-hidden="true"></i>
                </a>
            </td>
        </tr>
        <tr>
            <td>Revidēts gada pārskats</td>
            <td>no 899</td>
            <td>
                <a href="" data-openpopup="signuplogin" data-popup="contact-form" class="modal-toggle btn--cart btn--blue-border">
                    <i class="fa fa-cart-arrow-down fa-lg" aria-hidden="true"></i>
                </a>
            </td>
        </tr>
        <tr>
            <td>Zvērināta revidenta gada pārskata revīzija</td>
            <td>no 1499</td>
            <td>
                <a href="" data-openpopup="signuplogin" data-popup="contact-form" class="modal-toggle btn--cart btn--blue-border">
                    <i class="fa fa-cart-arrow-down fa-lg" aria-hidden="true"></i>
                </a>
            </td>
        </tr>
        <tr>
            <td>Ārvalstīs samaksātā PVN atgūšana</td>
            <td>no 49 (20% no atgūtā)</td>
            <td>
                <a href="" data-openpopup="signuplogin" data-popup="contact-form" class="modal-toggle btn--cart btn--blue-border">
                    <i class="fa fa-cart-arrow-down fa-lg" aria-hidden="true"></i>
                </a>
            </td>
        </tr>
    </tbody>
</table>

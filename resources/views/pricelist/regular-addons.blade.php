<table class="shopping-cart">
    <thead>
        <tr>
            <th>Papildus regulārie grāmatvedības pakalpojumi un atskaites</th>
            <th width="20%">Cena mēnesī bez pvn</th>
            <th width="10%">Pieteikt</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Papildus darbinieku algu aprēķins (par katriem 5 darbiniekiem)</td>
            <td>49</td>
            <td>
                <a href="javascript:void(0)" data-cb-type="portal" class="btn--cart btn--blue-border">
                    <i class="fa fa-cart-arrow-down fa-lg" aria-hidden="true"></i>
                </a>
            </td>
        </tr>
        <tr>
            <td>Papildus attālinātas grāmatvedības konsultācijas (par katru stundu)</td>
            <td>49</td>
            <td>
                <a href="javascript:void(0)" data-cb-type="portal" class="btn--cart btn--blue-border">
                    <i class="fa fa-cart-arrow-down fa-lg" aria-hidden="true"></i>
                </a>
            </td>
        </tr>
        <tr>
            <td>Autoratlīdzību deklarēšana (par katriem 10 autoriem)</td>
            <td>49</td>
            <td>
                <a href="javascript:void(0)" data-cb-type="portal" class="btn--cart btn--blue-border">
                    <i class="fa fa-cart-arrow-down fa-lg" aria-hidden="true"></i>
                </a>
            </td>
        </tr>
        <tr>
            <td>Atskaites Latvijas bankai</td>
            <td>49</td>
            <td>
                <a href="javascript:void(0)" data-cb-type="portal" class="btn--cart btn--blue-border">
                    <i class="fa fa-cart-arrow-down fa-lg" aria-hidden="true"></i>
                </a>
            </td>
        </tr>
        <tr>
            <td>Atskaites Centrālai statistikas pārvaldei</td>
            <td>49</td>
            <td>
                <a href="javascript:void(0)" data-cb-type="portal" class="btn--cart btn--blue-border">
                    <i class="fa fa-cart-arrow-down fa-lg" aria-hidden="true"></i>
                </a>
            </td>
        </tr>
        <tr>
            <td>Papildus 20 dokumentu pārbaude</td>
            <td>99</td>
            <td>
                <a href="javascript:void(0)" data-cb-type="portal" class="btn--cart btn--blue-border">
                    <i class="fa fa-cart-arrow-down fa-lg" aria-hidden="true"></i>
                </a>
            </td>
        </tr>
    </tbody>
</table>

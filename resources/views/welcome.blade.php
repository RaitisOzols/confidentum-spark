<!DOCTYPE html>
<html lang="lv" dir="ltr">
<head>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-154035955-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-154035955-1');
</script>

<meta http-equiv="Content-Type" content="text/html) charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<meta name="author" content="Confidentum" />
<meta name="description" content="Tiešsaistes grāmatvedības pakalpojumi jauniem un augošiem uzņēmumiem. Fiksētas grāmatvedības pakalpojumu cenas. Neierobežots operāciju skaits." />
<meta name="keywords" content="Grāmatvedības pakalpojumi" />
<title>Confidentum Spark - Tiešaistes Grāmatvedības Pakalpojumi</title>
<link rel="stylesheet" href="css/swiper.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/custom.css">
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,900" rel="stylesheet">

<script src="https://js.chargebee.com/v2/chargebee.js" data-cb-site="confidentum-spark" ></script>

<!-- Calendly link widget begin -->
<link href="https://assets.calendly.com/assets/external/widget.css" rel="stylesheet">
<script src="https://assets.calendly.com/assets/external/widget.js" type="text/javascript"></script>
<!-- Calendly link widget end -->

<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.css" />

<link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
<link rel="manifest" href="/favicon/site.webmanifest">

</head>
<body>

	@include('partials/header')

	@include('partials/intro')

	@include('partials/features')

	@include('partials/about')

	@include('partials/pricing')

	{{-- @include('partials/testimonials') --}}

	@include('partials/faq')

	@include('partials/support')

	@include('partials/clients')

	@include('partials/cta')

	@include('partials/footer')

	@include('modals/container')

	@include('modals/animation')

<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/jquery.paroller.min.js"></script>
<script src="js/jquery.custom.js"></script>
<script src="js/swiper.min.js"></script>
<script src="js/swiper.custom.js"></script>
<script src="js/menu.js"></script>
<script src="js/custom.js"></script>
@stack('js')

<script src="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.js" data-cfasync="false"></script>
<script>
window.cookieconsent.initialise({
  "palette": {
    "popup": {
      "background": "#1d8a8a"
    },
    "button": {
      "background": "#62ffaa"
    }
  },
  "theme": "classic",
  "content": {
    "message": "Šī tīmekļa vietne izmanto sīkdatnes, lai nodrošinātu optimālu darbību. Turpinot vietnes apskati, jūs piekrītat, ka izmantosim sīkdatnes jūsu ierīcē. Savu piekrišanu jūs jebkurā laikā varat atsaukt, nodzēšot saglabātās sīkdatnes.",
    "dismiss": "Piekrītu!",
    "link": "Lasīt vairāk",
    "href": "/privacy"
  }
});
</script>

<!-- Start of HubSpot Embed Code -->
<script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/6934008.js"></script>
<!-- End of HubSpot Embed Code -->

</body>
</html>

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { return view('welcome'); });
Route::get('/pricelist', function () { return view('pricelist.index'); });
Route::get('/quality', function () { return view('legal.quality'); });
Route::get('/privacy', function () { return view('legal.privacy'); });
Route::get('/vdar', function () { return view('legal.vdar'); });
Route::get('/contract', function () { return view('legal.contract'); });
// Route::get('/blog', function () { return view('blog.list'); });
//
// Route::get('/kontu-plans', function () { return view('blog.kontu-plans'); });

Route::post('contact-form', 'MailController@contact')->name('contact-form');
Route::post('support-form', 'MailController@support')->name('support-form');
